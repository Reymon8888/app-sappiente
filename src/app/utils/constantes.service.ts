import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })

export class Constantes {


public env = 'http://localhost:4100'; // url local env
//public env = 'https://afternoon-escarpment-09647.herokuapp.com'; // url prod env

public api = '/api';
public img = '/img/img-personal/';
public docs = '/doc-personal/';
public imgNoticia = '/img-noticias/'
public http = this.env + this.api;

public imagenesPathServer = this.env + this.img;
public documentosPathServer = this.env + this.docs;
public noticiasPathServer = this.env + this.imgNoticia;

}