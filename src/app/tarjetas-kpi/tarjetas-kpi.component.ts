import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { CatalogosService} from 'src/app/services/catalogos.service';
import { PersonalService } from 'src/app/services/personal.service';
import { ServiciosService} from 'src/app/services/servicios.service';

@Component({
  selector: 'app-tarjetas-kpi',
  templateUrl: './tarjetas-kpi.component.html',
  styleUrls: ['./tarjetas-kpi.component.css']
})
export class TarjetasKpiComponent implements OnInit {

  public idRol: any;

  public listaPlanteles = new Array<any>();
  public listaAulas = new Array<any>();
  public listaPlanes = new Array<any>();
  public listaPlazas = new Array<any>();
  public listaAsignaturas = new Array<any>();
  public listaPersonal = new Array<any>();
  public listaNoticias = new Array<any>();

  public docPath = this.serviciosService.noticiaPath;

  constructor( 
    private catService: CatalogosService,
    private spinner: NgxSpinnerService, 
    private personalService: PersonalService,
    private serviciosService: ServiciosService) { }

  ngOnInit(): void {

    this.idRol = localStorage.getItem('ID_ROL');

    this.getPlanteles();
    this.getAulas();
    this.getPlanes();
    this.getPlazas();
    this.getAsignaturas();
    this.getPersonal();
    this.getNoticias();
  }

  getNoticias(){
    this.spinner.show();
    this.serviciosService.obtieneNoticiasGeneral().subscribe(
      items => {
        this.listaNoticias = items;
      //  console.log(this.listaPlanteles);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getPlanteles(){
    this.spinner.show();
    this.catService.planteles().subscribe(
      items => {
        this.listaPlanteles = items;
      //  console.log(this.listaPlanteles);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getAulas(){
    this.spinner.show();
    this.catService.obtieneEspaciosEducativos().subscribe(
      items => {
        this.listaAulas = items;
     //   console.log(this.listaAulas);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getPlanes(){
    this.spinner.show();
    this.catService.obtienePlanesEstudio().subscribe(
      items => {
        this.listaPlanes = items;
     //   console.log(this.listaAulas);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getAsignaturas(){
    this.spinner.show();
    this.catService.obtieneAsignaturas().subscribe(
      items => {
        this.listaAsignaturas = items;
     //   console.log(this.listaAulas);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getPersonal(){
    this.spinner.show();
    this.personalService.obtienePersonal().subscribe(
      items => {
        this.listaPersonal = items.filter(pers => pers.cat_role.id !== 6 && pers.cat_role.id !== 7);
       // console.log(this.listaPersonal);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getPlazas(){
    this.spinner.show();
    this.personalService.obtienePlazas().subscribe(
      items => {
        this.listaPlazas = items.filter(item => item.tab_personal === null);
      // console.log(this.listaPlazas);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

}
