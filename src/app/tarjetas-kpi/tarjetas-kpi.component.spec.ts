import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetasKpiComponent } from './tarjetas-kpi.component';

describe('TarjetasKpiComponent', () => {
  let component: TarjetasKpiComponent;
  let fixture: ComponentFixture<TarjetasKpiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetasKpiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetasKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
