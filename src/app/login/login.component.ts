import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // variables y constantes

  mensajeError:string ;
  datosUsuario: any;

  constructor(private authService: AuthService, private router: Router,private spinner: NgxSpinnerService){

  }

  ngOnInit(): void {
    this.mensajeError = '';
  }

  onLogin(form): void{
    //console.log(form.value);
    this.spinner.show();
    this.authService.login(form.value).subscribe(res => {
      this.router.navigate(['dashboard']);
     
    }, error => {

      console.log(error.error.message);
      this.mensajeError = error.error.message; 
      this.spinner.hide();
    })
    
  }

}
