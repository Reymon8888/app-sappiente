import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AspectosEvaluacionComponent } from './aspectos-evaluacion.component';

describe('AspectosEvaluacionComponent', () => {
  let component: AspectosEvaluacionComponent;
  let fixture: ComponentFixture<AspectosEvaluacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AspectosEvaluacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AspectosEvaluacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
