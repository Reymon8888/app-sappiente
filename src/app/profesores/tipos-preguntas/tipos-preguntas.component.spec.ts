import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposPreguntasComponent } from './tipos-preguntas.component';

describe('TiposPreguntasComponent', () => {
  let component: TiposPreguntasComponent;
  let fixture: ComponentFixture<TiposPreguntasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposPreguntasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposPreguntasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
