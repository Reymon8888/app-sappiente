import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroCiclos'
})
export class FiltroCiclosPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if (arg === '' ) return value;
    const resultado = [];
    for ( const post of value){
      if (post.ciclo.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultado.push(post);
      };
    };
    return resultado;
  }
}
