import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroClases'
})
export class FiltroClasesPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if (arg === '' ) return value;
    const resultado = [];
    for ( const post of value){
      if (post.clave.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultado.push(post);
      };
    };
    return resultado;
  }
}
