import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatoNombreDoc'
})
export class FormatoNombreDocPipe implements PipeTransform {

  transform(value: string): string{
    let newStr: string = '';
      newStr += value.substr(15);
    return newStr;
  }

}
