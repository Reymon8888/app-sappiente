import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionAlumnoComponent } from './informacion-alumno.component';

describe('InformacionAlumnoComponent', () => {
  let component: InformacionAlumnoComponent;
  let fixture: ComponentFixture<InformacionAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
