import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenesAlumnoComponent } from './examenes-alumno.component';

describe('ExamenesAlumnoComponent', () => {
  let component: ExamenesAlumnoComponent;
  let fixture: ComponentFixture<ExamenesAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenesAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenesAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
