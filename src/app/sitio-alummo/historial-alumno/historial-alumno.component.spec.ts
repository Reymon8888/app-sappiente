import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialAlumnoComponent } from './historial-alumno.component';

describe('HistorialAlumnoComponent', () => {
  let component: HistorialAlumnoComponent;
  let fixture: ComponentFixture<HistorialAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
