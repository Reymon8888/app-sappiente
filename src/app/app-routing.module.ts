import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BienvenidaComponent } from './bienvenida/bienvenida.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard} from '../app/guards/auth.guard';
import {AdminGuard } from '../app/guards/admin.guard';
import {AlumnoGuard } from '../app/guards/alumno.guard';
import {DocenteGuard } from '../app/guards/docente.guard';

import { PlantelesComponent } from './planteles/planteles.component';

// configuraciones

import { CiclosComponent } from './configuraciones/ciclos/ciclos.component';
import { DepartamentosComponent } from './configuraciones/departamentos/departamentos.component';
import { EspaciosEducativosComponent } from './planteles/espacios-educativos/espacios-educativos.component';
import { TurnosComponent } from './configuraciones/turnos/turnos.component';
import { AsignaturasComponent } from './planes-estudio/asignaturas/asignaturas.component';
import { PlanesEstudioComponent } from './planes-estudio/planes-estudio.component';
import { PeriodosComponent } from './planes-estudio/periodos/periodos.component';
import { GruposAsignaturasComponent } from './planes-estudio/grupos-asignaturas/grupos-asignaturas.component';
import { PersonalComponent } from './personal/personal.component';
import { PlazasComponent } from './personal/plazas/plazas.component';
import { ActividadesComponent } from './servicios/actividades/actividades.component';
import { NoticiasComponent } from './servicios/noticias/noticias.component';
import { AspectosEvaluacionComponent } from './profesores/aspectos-evaluacion/aspectos-evaluacion.component';
import { ClasesComponent } from './profesores/clases/clases.component';
import { ExamenesComponent } from './profesores/examenes/examenes.component';
import { GruposComponent } from './alumnos/grupos/grupos.component';
import { AspirantesComponent } from './alumnos/aspirantes/aspirantes.component';
import { AlumnosComponent } from './alumnos/alumnos/alumnos.component';
import { EgresadosComponent } from './alumnos/egresados/egresados.component';
import { IngresosComponent } from './administracion/ingresos/ingresos.component';
import { AdeudosComponent } from './administracion/adeudos/adeudos.component';
import { BecasComponent } from './administracion/becas/becas.component';
import { NotasCreditoComponent } from './administracion/notas-credito/notas-credito.component';
import { DonativosComponent } from './administracion/donativos/donativos.component';
import { FacturacionComponent } from './administracion/facturacion/facturacion.component';
import { BancosComponent } from './administracion/bancos/bancos.component';
import { TiposPreguntasComponent } from './profesores/tipos-preguntas/tipos-preguntas.component';
import { InformacionAlumnoComponent } from './sitio-alummo/informacion-alumno/informacion-alumno.component';
import { ClasesAlumnoComponent } from './sitio-alummo/clases-alumno/clases-alumno.component';
import { ExamenesAlumnoComponent } from './sitio-alummo/examenes-alumno/examenes-alumno.component';
import { ActividadesAlumnoComponent } from './sitio-alummo/actividades-alumno/actividades-alumno.component';
import { HistorialAlumnoComponent } from './sitio-alummo/historial-alumno/historial-alumno.component';

const routes: Routes = [
  { path: '', redirectTo: 'bienvenida', pathMatch: 'full' },
  { path:'bienvenida', component: BienvenidaComponent },
  { path:'login', component: LoginComponent },

  { path:'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },

  { path:'planteles', component: PlantelesComponent, canActivate: [AuthGuard] },
  { path:'ciclos', component: CiclosComponent, canActivate: [AdminGuard] },
  { path:'espacios-educativos', component: EspaciosEducativosComponent, canActivate: [AdminGuard] },
  { path:'planes-estudio', component: PlanesEstudioComponent, canActivate: [AdminGuard] },
  { path:'grados', component: PeriodosComponent, canActivate: [AdminGuard] },
  { path:'grupos-asignaturas', component: GruposAsignaturasComponent, canActivate: [AdminGuard] },
  { path:'asignaturas', component: AsignaturasComponent, canActivate: [AdminGuard] },
  { path:'turnos', component: TurnosComponent, canActivate: [AdminGuard] },
  { path:'departamentos', component: DepartamentosComponent, canActivate: [AdminGuard] },
  { path:'personal', component: PersonalComponent, canActivate: [AdminGuard] },
  { path:'aspectos-evaluacion', component: AspectosEvaluacionComponent, canActivate: [AdminGuard] },
  { path:'clases', component: ClasesComponent, canActivate: [AdminGuard] },
  { path:'tipos-preguntas', component: TiposPreguntasComponent, canActivate: [AdminGuard] },
  { path:'examenes', component: ExamenesComponent, canActivate: [AdminGuard] },
  { path:'grupos', component: GruposComponent, canActivate: [AdminGuard] },
  { path:'aspirantes', component: AspirantesComponent, canActivate: [AdminGuard] },
  { path:'alumnos', component: AlumnosComponent, canActivate: [AdminGuard] },
  { path:'egresados', component: EgresadosComponent, canActivate: [AdminGuard] },
  { path:'plazas', component: PlazasComponent, canActivate: [AdminGuard] },
  { path:'actividades', component: ActividadesComponent, canActivate: [AdminGuard] },
  { path:'noticias', component: NoticiasComponent, canActivate: [AdminGuard] },
  { path:'ingresos', component: IngresosComponent, canActivate: [AdminGuard] },
  { path:'adeudos', component: AdeudosComponent, canActivate: [AdminGuard] },
  { path:'becas', component: BecasComponent, canActivate: [AdminGuard] },
  { path:'notas-credito', component: NotasCreditoComponent, canActivate: [AdminGuard] },
  { path:'donativos', component: DonativosComponent, canActivate: [AdminGuard] },
  { path:'facturacion', component: FacturacionComponent, canActivate: [AdminGuard] },
  { path:'bancos', component: BancosComponent, canActivate: [AdminGuard] },
  { path:'informacion-alumno', component: InformacionAlumnoComponent, canActivate: [AlumnoGuard] },
  { path:'clases-alumno', component: ClasesAlumnoComponent, canActivate: [AlumnoGuard] },
  { path:'examenes-alumno', component: ExamenesAlumnoComponent, canActivate: [AlumnoGuard] },
  { path:'actividades-alumno', component: ActividadesAlumnoComponent, canActivate: [AlumnoGuard] },
  { path:'historial-academico-alumno', component: HistorialAlumnoComponent, canActivate: [AlumnoGuard] },
  { path:'informacion-docente', component: InformacionAlumnoComponent, canActivate: [DocenteGuard] },
  { path:'clases-docente', component: ClasesComponent, canActivate: [DocenteGuard] },
  { path:'examenes-docente', component: ExamenesComponent, canActivate: [DocenteGuard] },
  { path: '**', component: BienvenidaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
