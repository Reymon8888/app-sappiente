import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogosService} from 'src/app/services/catalogos.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  nombreUsuario: string;
  institucion: string;
  idRol: any;

  constructor(private authService: AuthService, private catUsuariosService: CatalogosService,private spinner: NgxSpinnerService) { }


  logOut(): void{
    this.spinner.show();
      this.authService.logout();
      this.spinner.hide();
      this.idRol = localStorage.getItem('ID_ROL');
  }

  ngOnInit(): void {

    this.catUsuariosService.rolUsuario();
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.institucion = localStorage.getItem('INSTITUCION');
    this.spinner.hide();
  }

}
