import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ProfesoresService } from 'src/app/services/profesores.service';
import { AlumnosService } from 'src/app/services/alumnos.service';
import { PersonalService } from 'src/app/services/personal.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as moment from 'moment';
import { ExportsService } from 'src/app/services/exports.service';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {

  public formattedDate = (moment(new Date())).format('DD-MM-YYYY')
  public formattedTime = (moment(new Date())).format('HH:mm:ss')

  public logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC2BJREFUeNrsXb1y48gRHugUXGZudtlhw4tEZeeqq1oos6Ol/AIiM2cSowspho5IPQGxkTMvFToiFFzgSNjonAmbXXbwE9jT2oYL4moG84eZHhJdNaU/iMDMN939dffMgLFBBhlkkEEGGSSEJIfasb/8fDvmX0b4I3wd711S81a2fi7/8bfbegCYJpAZb98jiG1gTQRAr3j7hN8XMQOfRAzoe/zqQwDwgrf72ABPIgL1ircJbymBR9oi2FvqYCeEQQUzO+XtmgioIsl5+8CBLgaA1YAFMBeorSMWj4C/vuNA5wPAcmCnDv1m1fr+8yvXvJOwbJv7LqkAnRAAFgZ3ZQksmMeHhgHzwS0tnqVh4mdI4lILoOf8WbZHCzAf0BvUWl1TXLYYbeHBsrRZ+8hg8s34c1ZHAzCy4o2mWYQB+gCkJtRg4bNPWoxeR8Bs3x48wHyAblFrdUKSO2osFc35DYKtasbB8lz6nKCJ5wH5qJGcyHHWV4y48L5NcdKqAF2jb84PBmA0yTtF/7XFASAPrADolWI/wdXMogcYO71RNF9zqgkDA9O9UOzzRZ/ZsKTnzq6wsyQJCBEiWSLLLqMCmHdwoxDb9to5IkCrkMoaNbmMAmBFcNe8Q3N2BMLHI0OCOfINchIAXK8skphv3nWYbOcgJwHAvThkk0xtjE4cPvhNx4OXxw4uCIZGa8klz/kC1HgaALfivwFcNZCBe8hi4BTNubV84ygc+Dtv34aK9WKUX38pyh9+yqCEKcppf8f/nvLr7oMB3CIO3wkuqXj74wCuFGTgQZngkjFMArguCMD85qC5P0rIwp8DV35ggP7K24gP0r+JglyApkrYdcb//k9+3W9efTCWzWQls0sCPneCSYYN1nUpE69CQro2pp99amGaZTelllNunvfCUTzbpF9d1qYveXtkr1ekxpARM0nnmmrwRpKVgaWka4KKkmHa0JZQPqJVWLhiuqjFNYIskgXev1+AMe02kZCqGSFQKxeD1AoFd3sa5tTso0tbSi5Z+dBgmWmeEWPMlebzi8DdCKxW3oM/vpX44wwnWj8AY7ZKNGvXkdRyx1jGVPK3vIFJFg3qsqdnlFlBneVO6rloJBdPAt8LWvuWWryL7kTkJ9c4WUeC5Mx/2JddFSKu0WsNu6PMOFMt1uiw6BtJZ+cUkxlgUfhAyfojJGQdH10zeT7ZhcDnixb0LVTdg46Jvhb5uWMr/fmY0Pj5IheQqvriE0VzMZVpL1UUkDM4J26+JjTep7LxxScOtHdLFNxUl5AoytJzV2RaPLEGGOPGMZHO6ojq8lXtzzWNpXvQ4isXGhyd7+1IxtiKVW7YUO4Ev5905dhVABYN1AfC2rvo+fPHugkHS8mRuevg0w0w2viR5KZUfW92AJNon1FvTcx0lwa/F/x+S3hrybWn+yiRHIdyL7EmqSnAE82bURCfg/7OoxZvJWQr0wZ47yCxrzSYqHmGZ0493nLsuYuFpqWVarBoVlA+Ec73gGee73fvTIMl5oeyeU7ZYYtIg0ei2NxEgws2SCNeLRlazkLHep1IQo2R4CYkAcZy5pnn24ZYVCi655mOBqeEOqQCLhQVnjwz6FDu6kFHg091CRZBcFW2qvYlIaKJytpEc/mD4PefB3BfRBOV75tK1pqPdAAeU9dgXNIytdACqGPDnqmkafznt+zL0tVcgUCF5CKVYEwyVRNNgjVKwIWOmOaC56J126iR0Lb8HjABNhK//hAYYKWQ8FRTgysiCrwy/L8Xi9Vah4uPWhP4Oc/eLESXuIGQMXetSo5Pdew5hQIDlulMMlZFA27HcQpQ0N/iZKhh3xBajFRljDzJJ4FlSXUSHVTlyvD/2kXzjx2TZLLnApYsUokKYMtab6EQBrblJnA45EROI3te40SGSYHEdrPaoMH68s5S+3VDnEWrDQB7EJtyYJsJQ6xruzOhGAB2LzahyXVTUkN2DHHuGyRQJtFBFcOAReODX8vSaAqENY/8cwDQNYIMfhn87C2GX9eKVqKK5bjjE53Z6XPBd48C/vR3SGC0/TLEyLydsy/HPHSx5tCsWsRFSiuAWVzvMVLxyU8c5F3bOkC9mzfw0ZCXzgX/S3VNeG3rg1N2eALgAshP7cXsmK6EjdjnewNXEDg9aGwL8MMRAdzu2+YVoEv28nQeClktUSpZ2UTXruPQCIHeNQeC4sDljMAxFRKyWemQrPLANLhALXyjQZBgINvpyiWRA8zHvQLs6phbz3HnA5InsEyfTNgqobDoTMetnghsec0Mtkn0KZYDfGY4USgu8M90lPKkw6yR8cOWG72yluXZaoB8RwlZjNtTVwCLmPQkUP/eW7LOm5Z1umDyXPLzag6Ca8CFBEtk4U4NNBj8cBrAJ9lOLDjGsMKMFTz7xStLdp41geq5I5JJLpyISYdJeBKYhLnPA0fRPH909HFrZMRRHVKOLuZ3wZ8vRZOyK5NltKucmHnel+ddEJiLnkSE8cTA2nYCLMq5jj0XHlwzd9CGKfvydpNdJEUU2WkLtRHAmMER+VovRyV0MEdXk+cRNZok0DgGRqctqBQbRFo88ZT08DXoUwQaNHoaMKGjo0x111FWKgDnXaHHgQDc1mhY7P5VzTgguZpqciR1gDGkEH3QNbGZvj8xL7DVFlr91NOZlzqkcGSaiFGtB98F1OISWaJOma7GWm7Fut/6+SJhIGCkK9zCEkJ7ReZZqS6tBDBmdMoQWgzxHW+6WpjjM7XBFR0m1u5Xhfe6fOV+0wBhlUx7lSa8zooOmRb7WDesczwDEMPmBRo5JgLe4FKc2d4EOt8fLEwaXLQmwBwBX3nU3rRDewunACNbE2nxjYcQQ/XzG1K4RFBn7SwP9mPWThDg0fxv2xFD6w0oNWbt4O+FRy1e2WoviO6y2TkTvwNhxRy8eMoBwEskhqVssnKgQCOv9shk/spkqfHv8HXm4xDSjrfKFTpFkMTg5jtJZqm3HDW/739VtBfJVbTS8fITkHOdRX8mOxukr3wJmA2qWcTbPFsie6tcrruiUxtgNGVLCeHaBIqNl7HsNpBo71Rimmtm8H4Mo71JSEpKia/sg20WHaZ5HTm4XeNm9FY5m81nMlM97SH7c4kkbj9GPRS/u2PyF34aLUJILB/sRmHW5T3FiCmGMOWBgCtbDntuukAhcfCAH5m8GH0eOwg9A/zYEQJajZ+L/cFNzlckuwPZldgHuJsOcGe2ypE4etBxhw95Xsk4aPILswyubdp3TJ84fOiMdb8Re3aE7znU9bkNqbp0cT9nRzhg+qxrxm0C11ZDg5sqgFsyh29RT3roBJidrtopaPE8tqWrDixcV226RFfmbFy+cd2RX38pyh9+yj53MGuYwX/i1/2LX//bEYB7i5P+W5/g9qLBmpr8nD+OPQvVYZJhDLKOS3sBt1eANcwSSIEErDogcIFrLBT63qu7Sjx0dIwgpwqX//+Io8h97Yqp1a/XfW8qTzx1ulkflSlcXqHZziMDNkVgVVZ81Ki1vfcx8TwIt0x9/VaFGr2lrNFooa6Z+usFSuYgQ0USYAOT3cx2mOl3lHw0ksgrprdvau37nI8k0OA0KzF1kx4w62F1ZxECbFwr9R7N8EjzuechNpQngbWgKXJnBv8Og3bPNBehGfhVeLZ3BqA21ucOF0gEkaAA72nGitntIizQb3/G75VrxWhRmlfTpgjomNkd3dhr+BMVwHt+bcHcbxet2ddLjFLWz7bUnBFaH0YKYEsCE1Jq5AY5tWRNQnnU0EdfYQhCcRcjuIIPlGP2JBINsWGwfYB6j/F5RX3cogH4Fc1u2G3WM+DN9tUHZOxRpVGjBFgQzoyxfd8iUKmmZjIE8pmUUX0Z9tEBrDgJ9glbdUjVq0EGGWSQQQYZhJb8T4ABAAV05c/Eo4lWAAAAAElFTkSuQmCC';

  public institucion: string;
  public idInstitucion = Number(localStorage.getItem('ID_INSTITUCION'));

  public nombreUsuario: any;
  public idUsuario: number;
  public listaGrupos = new Array<any>();
  public listaCiclos = new Array<any>();
  public listaTurnos = new Array<any>();
  public listaProfesores = new Array<any>();
  public listaAlumnos = new Array<any>();
  public listaAlumnosNoAsignados = new Array<any>();
  public listaPlanteles = new Array<any>();
  public listaGrados = new Array<any>();
  public idItemSeleccionado: number;
  public registroSeleccionado = new Array<any>();
  public grupoSeleccionado: any;
  public rolAdm: any;
  public filtroGrupo = '';
  public aspectoIndex: number;

  public formGrupo: FormGroup;
  public formAlumno: FormGroup;
  public validador = '';
  public edicion: boolean;

  public itemsPerPage: number = 10; // numero de registros por pagina
  public showDatos: boolean = false;
  public paginadorshow: boolean = false;
  public p;

  public confirmaGuardado: boolean;
  public confirmaEdicion: boolean;

  constructor(
    private authService: AuthService,
    private catService: CatalogosService,
    private profesoresService: ProfesoresService,
    private alumnosService: AlumnosService,
    private personalService: PersonalService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private exportExcel: ExportsService) { }


  logOut(): void {
    this.authService.logout();
  }


  ngOnInit(): void {

    this.edicion = false;
    this.catService.rolUsuario();
    this.idUsuario = Number(localStorage.getItem('ID_USUARIO'));
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.institucion = localStorage.getItem('INSTITUCION');
    this.rolAdm = localStorage.getItem('ROL');
    this.getGrupos();
    this.getCiclos();
    this.getTurnos();
    this.getPlanteles();
    this.getGrados();
    this.getProfesores();
    this.formGrupo = this.formBuilder.group({
      grupo: new FormControl(null, Validators.required),
      maximoAlumnos: new FormControl(null, Validators.required),
      profesor: new FormControl(null, Validators.required),
      plantel: new FormControl(null, Validators.required),
      grado: new FormControl(null, Validators.required),
      ciclo: new FormControl(null, Validators.required),
      turno: new FormControl(null, Validators.required)
    });
    this.formAlumno = this.formBuilder.group({
      alumno: new FormControl(null, Validators.required)
    }) 
  }

  // setea formulario para nuevo registro

  public nuevo() {
    this.edicion = false;
    this.formGrupo.reset();
    this.formGrupo.controls.ciclo.setValue(0);
    this.formGrupo.controls.profesor.setValue('');
    this.formGrupo.controls.plantel.setValue('');
    this.formGrupo.controls.grado.setValue('');
    this.formGrupo.controls.ciclo.setValue('');
    this.formGrupo.controls.turno.setValue('');
  }

  // lista de ciclos

  getGrupos() {

    this.spinner.show();
    this.alumnosService.obtieneGrupos().subscribe(
      items => {
        this.listaGrupos = items;
        //console.group(items)
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getAlumnos(grupo){
    this.spinner.show();
    this.formAlumno.controls.alumno.setValue('');
    this.alumnosService.obtieneAlumnos().subscribe(
      items => {
        //console.log(items)
        
        this.listaAlumnosNoAsignados = items.filter(items => items.fk_alumno_grupo === null)
        const alumnosAsignados = items.filter(items => items.fk_alumno_grupo !== null);

        this.listaAlumnos = alumnosAsignados.filter(items => items.tab_grupo.id === grupo);
        //console.group(this.listaAlumnos)
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  agregarAlumno(grupo){
    this.spinner.show();

    const idItemSeleccionado = this.formAlumno.controls.alumno.value;
    this.alumnosService.actualizaAlumno( // Envía método post al servicio con los valores del formulario
      {
        id: idItemSeleccionado,
        rol: 'Alumno',
        grupo: grupo,
        registradoPor: this.idUsuario
      }
    )
      .subscribe(response => { // Se completa el registro
        this.formGrupo.reset(); // Limpia campos del formulario
        this.getAlumnos(grupo);
        this.p = 1;
        this.spinner.hide();
      });
  }

  eliminarAlumno(alumno, grupo){
    this.spinner.show();
    this.alumnosService.actualizaAlumno( // Envía método post al servicio con los valores del formulario
      {
        id: alumno,
        rol: 'Aspirante',
        grupo: null,
        registradoPor: this.idUsuario
      }
    )
      .subscribe(response => { // Se completa el registro
        this.formGrupo.reset(); // Limpia campos del formulario
        this.getAlumnos(grupo);
        this.p = 1;
        this.spinner.hide();
      });
  }

  getCiclos() {

    this.spinner.show();
    this.catService.obtieneCiclos().subscribe(
      items => {
        this.listaCiclos = items;
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getTurnos() {

    this.spinner.show();
    this.catService.obtieneTurnos().subscribe(
      items => {
        this.listaTurnos = items;
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getProfesores() {

    this.spinner.show();
    this.personalService.obtienePersonal().subscribe(
      items => {
        this.listaProfesores = items.filter(items => items.rol === 'Profesor');
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getPlanteles() {

    this.spinner.show();
    this.catService.planteles().subscribe(
      items => {
        this.listaPlanteles = items;
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getGrados() {

    this.spinner.show();
    this.catService.obtienePeriodos().subscribe(
      items => {
        this.listaGrados = items;
        this.spinner.hide();
      }, error => {
        if (error.status === 500) {
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // guarda ciclo

  public guardarNuevoGrupo(): void {
    this.spinner.show();
    if (this.formGrupo.valid) {

      this.alumnosService.creaGrupo(
        {
          grupo: this.formGrupo.controls.grupo.value,
          maximoAlumnos: this.formGrupo.controls.maximoAlumnos.value,
          ciclo: this.formGrupo.controls.ciclo.value,
          turno: this.formGrupo.controls.turno.value,
          plantel: this.formGrupo.controls.plantel.value,
          profesor: this.formGrupo.controls.profesor.value,
          grado: this.formGrupo.controls.grado.value,
          institucion: this.idInstitucion,
          registradoPor: this.idUsuario
        })
        .subscribe(response => {
          window.scroll(0, 0);
          this.formGrupo.reset();
          this.formGrupo.controls.maximoAlumnos.setValue(0);
          this.formGrupo.controls.profesor.setValue('');
          this.formGrupo.controls.ciclo.setValue('');
          this.formGrupo.controls.turno.setValue('');
          this.formGrupo.controls.grado.setValue('');
          this.formGrupo.controls.plantel.setValue('');
          this.getGrupos();
         // console.log(response);
          this.confirmaGuardado = true;
          this.spinner.hide();

          setTimeout(() => {
            this.confirmaGuardado = false;
          }, 2500);
        });
    }
    else {
      this.validador = 'Favor de llenar todos los campos';
    }
  }

  // funcion para editar registro
  public updateGrupo(id) {
    this.spinner.show();

    this.edicion = true;

    this.idItemSeleccionado = id;
    this.registroSeleccionado = this.listaGrupos.filter(id => id.id === this.idItemSeleccionado);
    this.grupoSeleccionado = this.registroSeleccionado.values;

    for (const grupoSeleccionado of this.registroSeleccionado) {
      this.formGrupo.controls.grupo.setValue(grupoSeleccionado.grupo);
      this.formGrupo.controls.maximoAlumnos.setValue(grupoSeleccionado.maximo_alumnos);
      this.formGrupo.controls.ciclo.setValue(grupoSeleccionado.cat_ciclo.id);
      this.formGrupo.controls.turno.setValue(grupoSeleccionado.cat_turno.id);
      this.formGrupo.controls.plantel.setValue(grupoSeleccionado.cat_plantele.id);
      this.formGrupo.controls.profesor.setValue(grupoSeleccionado.tab_personal.id);
      this.formGrupo.controls.grado.setValue(grupoSeleccionado.cat_periodos_escolare.id);
      this.idItemSeleccionado = grupoSeleccionado.id;
    }
    this.spinner.hide();
  }

  // función para guardar cambios
  public guardarCambios(): void {
    this.spinner.show();
    if (this.formGrupo.valid) { // Recibe objeto de formulario y valida
      this.alumnosService.actualizaGrupo( // Envía método post al servicio con los valores del formulario
        {
          id: this.idItemSeleccionado,
          grupo: this.formGrupo.controls.grupo.value,
          maximoAlumnos: this.formGrupo.controls.maximoAlumnos.value,
          ciclo: this.formGrupo.controls.ciclo.value,
          turno: this.formGrupo.controls.turno.value,
          plantel: this.formGrupo.controls.plantel.value,
          profesor: this.formGrupo.controls.profesor.value,
          grado: this.formGrupo.controls.grado.value,
          registradoPor: this.idUsuario
        }
      )
        .subscribe(response => { // Se completa el registro
          this.formGrupo.reset(); // Limpia campos del formulario
          this.confirmaEdicion = true;
          setTimeout(() => {
            this.confirmaEdicion = false;
            this.edicion = false;
          }, 2500);
          this.getGrupos();
          this.formGrupo.controls.maximoAlumnos.setValue(0);
          this.formGrupo.controls.profesor.setValue('');
          this.formGrupo.controls.ciclo.setValue('');
          this.formGrupo.controls.turno.setValue('');
          this.formGrupo.controls.grado.setValue('');
          this.formGrupo.controls.plantel.setValue('');
          this.spinner.hide();
        });
    } else {
      this.validador = 'Favor de llenar todos los campos'; // Si hay campos vacíos muestra alerta
      this.spinner.hide();
    }
  }

  // función para desactivar registro
  public eliminarGrupo(id): void {
    this.spinner.show();

    this.idItemSeleccionado = id;
    this.alumnosService.actualizaGrupo( // Envía método post al servicio con los valores del formulario
      {
        id: this.idItemSeleccionado,
        estatus: 0,
        registradoPor: this.idUsuario
      }
    )
      .subscribe(response => { // Se completa el registro
        this.formGrupo.reset(); // Limpia campos del formulario
        this.getGrupos();
        this.p = 1;
        this.spinner.hide();
      });
  }


  // funcion para imprimir PDF

  public descargarPdf() {

    var doc = new jsPDF('p', 'mm', 'A4')
    // cabeceras del reporte

    const addHeaders = doc => {
      const pageCount = doc.internal.getNumberOfPages()

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {

        doc.setPage(i)
        autoTable(doc, {
          theme: 'grid',
          margin: { top: 15 },
          head: [[{ content: 'SAPP - Lista de grupos', colSpan: 4, styles: { halign: 'center', minCellHeight: 14, fillColor: '#fff', fontSize: 18, textColor: '#000' } }]],
          body: [
            [{ content: 'Instituto: ', styles: { fontStyle: 'bold' } }, this.institucion, { content: 'Fecha: ', styles: { fontStyle: 'bold' } }, this.formattedDate],
            [{ content: 'Usuario: ', styles: { fontStyle: 'bold' } }, this.nombreUsuario, { content: 'Hora: ', styles: { fontStyle: 'bold' } }, this.formattedTime],
            // ...
          ],
        })
        doc.text('Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 287, {
          align: 'center'
        })
        
        doc.addImage(this.logo,'png',180, 3,20,20)
      }
    }

      // datos del reporte

      addHeaders(doc)
      autoTable(doc, { html: '#reportePdf', margin: { top: 50 }, headStyles: { fillColor: '#527780' } })
      const newHead = doc => {
        const pageCount = doc.internal.getNumberOfPages()
        if(pageCount > 1) {
          addHeaders(doc)
        }
      }
      newHead(doc)
      doc.save('Grupos.pdf');

  }

  public imprimirLista(grupo) {

    var doc = new jsPDF('p', 'mm', 'A4')
    // cabeceras del reporte
    const grupoSel = this.listaGrupos.filter(items => items.id === grupo);
    var gSel = '';
    var prof = '';
    var plan = '';
    var grad = '';
    var cic= '';
    var turn = '';
    grupoSel.forEach(it => {
      gSel = it.grupo;
      prof = it.tab_personal.nombres +' '+ it.tab_personal.apellido_pat +' '+it.tab_personal.apellido_mat;
      plan = it.cat_plantele.nombre_plantel;
      grad = it.cat_periodos_escolare.periodo;
      cic = it.cat_ciclo.ciclo;
      turn = it.cat_turno.turno;

    })
    const addHeaders = doc => {
      const pageCount = doc.internal.getNumberOfPages()

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {

        doc.setPage(i)
        autoTable(doc, {
          theme: 'grid',
          margin: { top: 15 },
          head: [[{ content: 'Lista de alumnos', colSpan: 4, styles: { halign: 'center', minCellHeight: 14, fillColor: '#fff', fontSize: 18, textColor: '#000' } }]],
          body: [
            [{ content: 'Instituto: ', styles: { fontStyle: 'bold' } }, this.institucion, { content: 'Plantel: ', styles: { fontStyle: 'bold' } }, plan],
            [{ content: 'Grado: ', styles: { fontStyle: 'bold' } }, grad, { content: 'Grupo: ', styles: { fontStyle: 'bold' } }, gSel],
            [{ content: 'Profesor: ', styles: { fontStyle: 'bold' } }, prof, { content: 'Fecha: ', styles: { fontStyle: 'bold' } }, this.formattedDate +' '+this.formattedTime],
            [{ content: 'Ciclo escolar: ', styles: { fontStyle: 'bold' } }, cic, { content: 'Turno: ', styles: { fontStyle: 'bold' } }, turn],
          ],
        })
        doc.text('Página ' + String(i) + ' de ' + String(pageCount) + ' - Grupo ' + gSel, doc.internal.pageSize.width / 2, 291, {
          align: 'center'
        })
        
        doc.addImage(this.logo,'png',180, 3,20,20)
      }

      
      
      
    }

    

      // datos del reporte

      addHeaders(doc)
      autoTable(doc, {theme: 'grid', html: '#listaAlumnosPdf', margin: { top: 50 }, headStyles: { fillColor: '#527780', halign: 'center' }, styles: {fontStyle: 'bold'} })
      const newHead = doc => {
        const pageCount = doc.internal.getNumberOfPages()
        if(pageCount > 1) {
          addHeaders(doc)
        }
      }
      newHead(doc)
      doc.save('Grupo_'+ gSel +'.pdf');

  }

  // exporta excel

  public exportaExcel(): void{
    this.exportExcel.exportToExcel(this.listaGrupos, 'my_export');
  }


}
