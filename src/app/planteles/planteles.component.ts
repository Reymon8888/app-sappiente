import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogosService} from 'src/app/services/catalogos.service';
import { FormControl, FormGroup, Validators, FormBuilder, DefaultValueAccessor } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-planteles',
  templateUrl: './planteles.component.html',
  styleUrls: ['./planteles.component.css']
})
export class PlantelesComponent implements OnInit {

  public institucion: string;
  public idInstitucion = Number(localStorage.getItem('ID_INSTITUCION'));

  public nombreUsuario: any;
  public idUsuario: number;
  public listaEspacios = new Array<any>();
  public listaPlanteles = new Array<any>();
  public listaPlanesEstudio = new Array<any>();
  public plantel = "";
  public idItemSeleccionado: number;
  public registroSeleccionado = new Array<any>();
  public plantelSeleccionado: any;
  public rolAdm:any;
  public filtroPlantel = '';

  public formPlantel: FormGroup;
  public validador = '';
  public edicion: boolean;

  public itemsPerPage: number = 10; // numero de registros por pagina
  public showDatos: boolean = false;
  public paginadorshow: boolean = false;
  public p;

  public confirmaGuardado: boolean;
  public confirmaEdicion: boolean;

  public paso1 = true;
  public paso2 = false;
  public paso3 = false;
  public paso4 = false;
  public paso5 = false;

  public activo = '';


  constructor(private authService: AuthService, private catService: CatalogosService,private formBuilder: FormBuilder,private spinner: NgxSpinnerService, private router: Router) { }


  logOut(): void{
      this.authService.logout();
  }

  ngOnInit(): void {
    

    //window.location.reload();

    this.edicion = false;
    this.catService.rolUsuario();
    this.idUsuario = Number(localStorage.getItem('ID_USUARIO'));
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.institucion = localStorage.getItem('INSTITUCION');
    this.rolAdm = localStorage.getItem('ROL');
    this.getEspacios();
    this.getPlanteles();
    this.getPlanesEstudio();
    this.formPlantel = this.formBuilder.group({
    clave: new FormControl(null, Validators.required),
    nombrePlantel: new FormControl(null, Validators.required),
    alias: new FormControl(null, Validators.required),
    domicilio: new FormControl(null, Validators.required),
    cp: new FormControl(null, Validators.required),
    colonia: new FormControl(null, Validators.required),
    ciudad: new FormControl(null, Validators.required),
    municipio: new FormControl(null, Validators.required),
    entidad: new FormControl(null, Validators.required),
    telefono1: new FormControl(null, Validators.required),
    telefono2: new FormControl(null),
    correo: new FormControl(null, Validators.required),
    directorGeneral: new FormControl(null, Validators.required),
    directorAcademico: new FormControl(null, Validators.required),
    directorAdministrativo: new FormControl(null, Validators.required),
    cct: new FormControl(null),
    seccion: new FormControl(null)
    });
    this.stepper1();
  }

  // stepper

  public stepper1(){
    this.paso1 = true;
    this.paso2 = false;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''

  }

  public stepper2(){

    if(
      this.formPlantel.controls.clave.value == '' ||
      this.formPlantel.controls.nombrePlantel.value == '' ||
      this.formPlantel.controls.alias.value == '' ||
      this.formPlantel.controls.clave.value == null ||
      this.formPlantel.controls.nombrePlantel.value == null ||
      this.formPlantel.controls.alias.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
    this.paso2 = true;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''
    }
  }

  public stepper3(){

    if(
      this.formPlantel.controls.domicilio.value == '' ||
      this.formPlantel.controls.cp.value == '' ||
      this.formPlantel.controls.colonia.value == '' ||
      this.formPlantel.controls.ciudad.value == '' ||
      this.formPlantel.controls.municipio.value == '' ||
      this.formPlantel.controls.entidad.value == '' ||
      this.formPlantel.controls.domicilio.value == null ||
      this.formPlantel.controls.cp.value == null ||
      this.formPlantel.controls.colonia.value == null ||
      this.formPlantel.controls.ciudad.value == null ||
      this.formPlantel.controls.municipio.value == null ||
      this.formPlantel.controls.entidad.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
    this.paso1 = false;
    this.paso2 = false;
    this.paso3 = true;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''
    }

  }

  public stepper4(){

    if(
      this.formPlantel.controls.telefono1.value == '' ||
      this.formPlantel.controls.correo.value == '' ||
      this.formPlantel.controls.telefono1.value == null ||
      this.formPlantel.controls.correo.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
      this.paso2 = false;
      this.paso3 = false;
      this.paso4 = true;
      this.paso5 = false;
    this.validador = ''
    }
  }

  public stepper5(){

    if(
      this.formPlantel.controls.directorGeneral.value == '' ||
      this.formPlantel.controls.directorAcademico.value == '' ||
      this.formPlantel.controls.directorAdministrativo.value == '' ||
      this.formPlantel.controls.directorGeneral.value == null ||
      this.formPlantel.controls.directorAcademico.value == null ||
      this.formPlantel.controls.directorAdministrativo.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
    this.paso2 = false;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = true;
    this.validador = ''
    }
  }

  // setea formulario para nuevo registro

  public nuevo(){
    this.edicion = false;
    this.stepper1();
    this.formPlantel.reset();
    this.validador = '';
    this.activo = 'true';
  }

  getPlanteles(){
    this.spinner.show();
    this.catService.planteles().subscribe(
      items => {
        this.listaPlanteles = items;
        //console.log(this.listaPlanteles.length + ' registros');
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // lista de espacios educativos

  getEspacios(){
    this.spinner.show();
    this.catService.obtieneEspaciosEducativos().subscribe(
      items => {
        this.listaEspacios = items;
       // console.log(this.listaEspacios);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // lista de planes

  getPlanesEstudio(){
    this.spinner.show();
    this.catService.obtienePlanesEstudio().subscribe(
      items => {
        this.listaPlanesEstudio = items;
       // console.log(this.listaEspacios);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // guarda espacio educativo

  public guardarNuevoPlantel(): void {
    this.spinner.show();
    this.validador = '';
    if(this.formPlantel.valid) {

    this.catService.crearPlantel(
      {
        clave: this.formPlantel.controls.clave.value,
        nombrePlantel: this.formPlantel.controls.nombrePlantel.value,
        alias: this.formPlantel.controls.alias.value,
        domicilio: this.formPlantel.controls.domicilio.value,
        cp: this.formPlantel.controls.cp.value,
        colonia: this.formPlantel.controls.colonia.value,
        ciudad: this.formPlantel.controls.ciudad.value,
        municipio: this.formPlantel.controls.municipio.value,
        entidad: this.formPlantel.controls.entidad.value,
        telefono1: this.formPlantel.controls.telefono1.value,
        telefono2: this.formPlantel.controls.telefono2.value,
        correo: this.formPlantel.controls.correo.value,
        directorGeneral: this.formPlantel.controls.directorGeneral.value,
        directorAcademico: this.formPlantel.controls.directorAcademico.value,
        directorAdministrativo: this.formPlantel.controls.directorAdministrativo.value,
        cct: this.formPlantel.controls.cct.value,
        seccion: this.formPlantel.controls.seccion.value,
        institucion: this.idInstitucion,
        registradoPor: this.idUsuario
      })
    .subscribe(response => {
      window.scroll(0, 0);
      this.formPlantel.reset();
      this.getPlanteles();
      //console.log(response);
      this.confirmaGuardado = true;
      this.spinner.hide();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.confirmaGuardado = false;
      this.stepper1();
    }, 2500);
    });
    }
    else {
      this.validador = 'Favor de llenar los campos obligatorios';
     // console.log(this.validador)
      this.spinner.hide();
    }
  }

  // funcion para editar registro
  public updatePlantel(id) {
    this.spinner.show();
    this.stepper1();
    this.validador = '';
    this.edicion = true;

    //console.log(id);
    this.idItemSeleccionado = id;
    this.registroSeleccionado = this.listaPlanteles.filter(id => id.id === this.idItemSeleccionado);

    this.plantelSeleccionado = this.registroSeleccionado.values;

    for (const plantelSeleccionado of this.registroSeleccionado) {
      //console.log(plantelSeleccionado);
      //console.log(plantelSeleccionado.descripcion);
      this.formPlantel.controls.clave.setValue(plantelSeleccionado.clave);
      this.formPlantel.controls.nombrePlantel.setValue(plantelSeleccionado.nombre_plantel);
      this.formPlantel.controls.alias.setValue(plantelSeleccionado.alias);
      this.formPlantel.controls.domicilio.setValue(plantelSeleccionado.domicilio);
      this.formPlantel.controls.cp.setValue(plantelSeleccionado.cp);
      this.formPlantel.controls.colonia.setValue(plantelSeleccionado.colonia);
      this.formPlantel.controls.ciudad.setValue(plantelSeleccionado.ciudad);
      this.formPlantel.controls.municipio.setValue(plantelSeleccionado.municipio);
      this.formPlantel.controls.entidad.setValue(plantelSeleccionado.entidad);
      this.formPlantel.controls.telefono1.setValue(plantelSeleccionado.telefono1);
      this.formPlantel.controls.telefono2.setValue(plantelSeleccionado.telefono2);
      this.formPlantel.controls.correo.setValue(plantelSeleccionado.correo);
      this.formPlantel.controls.directorGeneral.setValue(plantelSeleccionado.director_general);
      this.formPlantel.controls.directorAcademico.setValue(plantelSeleccionado.director_academico);
      this.formPlantel.controls.directorAdministrativo.setValue(plantelSeleccionado.director_administrativo);
      this.formPlantel.controls.cct.setValue(plantelSeleccionado.cct);
      this.formPlantel.controls.seccion.setValue(plantelSeleccionado.seccion);

      this.idItemSeleccionado = plantelSeleccionado.id;
    }



    //console.log(this.registroSeleccionado);
    this.spinner.hide();

  }

  // función para guardar cambios
  public guardarCambios(): void {
    this.spinner.show();
    //console.log(this.formPlantel.value);


    if (this.formPlantel.valid) { // Recibe objeto de formulario y valida
    //console.log(this.idItemSeleccionado);
    this.catService.editarPlantel( // Envía método post al servicio con los valores del formulario
      {
        id: this.idItemSeleccionado,
        clave: this.formPlantel.controls.clave.value,
        nombrePlantel: this.formPlantel.controls.nombrePlantel.value,
        alias: this.formPlantel.controls.alias.value,
        domicilio: this.formPlantel.controls.domicilio.value,
        cp: this.formPlantel.controls.cp.value,
        colonia: this.formPlantel.controls.colonia.value,
        ciudad: this.formPlantel.controls.ciudad.value,
        municipio: this.formPlantel.controls.municipio.value,
        entidad: this.formPlantel.controls.entidad.value,
        telefono1: this.formPlantel.controls.telefono1.value,
        telefono2: this.formPlantel.controls.telefono2.value,
        correo: this.formPlantel.controls.correo.value,
        directorGeneral: this.formPlantel.controls.directorGeneral.value,
        directorAcademico: this.formPlantel.controls.directorAcademico.value,
        directorAdministrativo: this.formPlantel.controls.directorAdministrativo.value,
        cct: this.formPlantel.controls.cct.value,
        seccion: this.formPlantel.controls.seccion.value,
        registradoPor: this.idUsuario
      }
    )
    .subscribe(response => { // Se completa el registro
      this.formPlantel.reset(); // Limpia campos del formulario
     // console.log('bla v+ssasa' + this.formPlantel);
      this.confirmaEdicion = true;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.confirmaEdicion = false;
        this.edicion = false;
        this.stepper1();
      }, 2500);
      this.getPlanteles();
      this.spinner.hide();
    });
    } else {
      this.validador = 'Favor de llenar todos los campos'; // Si hay campos vacíos muestra alerta
      this.spinner.hide();
    }
  }

  // función para desactivar registro
  public eliminarPlantel(id): void {
    this.spinner.show();

    this.idItemSeleccionado = id;
    //console.log(this.idItemSeleccionado);
    this.catService.editarPlantel( // Envía método post al servicio con los valores del formulario
      {
        id: this.idItemSeleccionado,
        estatus: 0,
        registradoPor: this.idUsuario
      }
    )
    .subscribe(response => { // Se completa el registro
      this.formPlantel.reset(); // Limpia campos del formulario
      //console.log('bla v+ssasa' + this.formPlantel);
      this.getPlanteles();
      this.p = 1;
      this.spinner.hide();
    });
  }

}
