import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspaciosEducativosComponent } from './espacios-educativos.component';

describe('EspaciosEducativosComponent', () => {
  let component: EspaciosEducativosComponent;
  let fixture: ComponentFixture<EspaciosEducativosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspaciosEducativosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspaciosEducativosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
