export interface JwtResponseInterface {
    id: number,
    usuario: string,
    correo: string,
    token: string,
    rolId: [],
    auth: boolean
}