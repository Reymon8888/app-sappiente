import { LowerCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})
export class BarraComponent implements OnInit {

  public linkLogin: any;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

  this.linkLogin = localStorage.getItem('AUTH');

  }

}
