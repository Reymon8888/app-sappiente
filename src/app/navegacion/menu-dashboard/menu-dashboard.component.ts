import { Component, OnInit, Input } from '@angular/core';
import { CatalogosService } from '../../services/catalogos.service';
import { ToggleService } from '../../services/toggle.service';
import { AuthService } from '../../services/auth.service';
import { RouterLink } from '@angular/router';
import { RadioControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-menu-dashboard',
  templateUrl: './menu-dashboard.component.html',
  styleUrls: ['./menu-dashboard.component.css']
})
export class MenuDashboardComponent implements OnInit {

  @Input() valorRecibido: string = '';

  public nombreUsuario: any;
  public idUsuario: any;
  public idRol = localStorage.getItem('ID_ROL');
  public listaItems = new Array<any>();
  public mainItems = new Array<any>();
  public segundoNivelItems = new Array<any>();
  public menuConstruido = new Array<any>();
  public idItemCollapse: string;
  public muestraIconoCollapse: string;

  public menu: any;
  public muestraMenu: string;

  constructor(private catService: CatalogosService, public toggleService: ToggleService, private authService: AuthService) { }

  ngOnInit(): void {

    this.muestraMenu = '';
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.getMenuDinamico();
    this.showIcon();
  }


  showIcon(){
    if(this.idRol === '2' ){
      this.muestraIconoCollapse = 'collapse';

    }else{
      this.muestraIconoCollapse = '';
    }
  }

  getMenuDinamico(){
    this.catService.menuDinamico().subscribe(
      itemsMenu => {
        this.listaItems = itemsMenu;
        this.mainItems = this.listaItems.filter(nivel => nivel.nivel === 1);
        this.segundoNivelItems = this.listaItems.filter(nivel => nivel.nivel === 2);
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
        }
      }
    )
  }

  public ocultarMenu(){

    if(this.toggleService.estatus === ''){
      this.toggleService.estatus = 'toggled';
    }else{
      this.toggleService.estatus = '';
    }
    
  }

}
