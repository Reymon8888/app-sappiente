import { Component, OnInit } from '@angular/core';
import { CatalogosService } from '../../services/catalogos.service';
import { ToggleService } from '../../services/toggle.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-barra-dashboard',
  templateUrl: './barra-dashboard.component.html',
  styleUrls: ['./barra-dashboard.component.css']
})
export class BarraDashboardComponent implements OnInit {


  public nombreUsuario: any;
  public idUsuario: any;
  public rolUsuario: any;
  public detalleRol = new Array<any>();

  public menu: any;
  public muestraMenu: string;

  constructor(private catService: CatalogosService, public toggleService: ToggleService, private authService: AuthService) { }


  ngOnInit(): void {

    this.muestraMenu = '';
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.rolUsuario = localStorage.getItem('ROL');
    this.idUsuario = localStorage.getItem('ID_USUARIO');
    this.getRolUsuario();
  }

  getRolUsuario(){
    this.catService.rolUsuario().subscribe(
      detalleRoll => {
        this.detalleRol = [detalleRoll];
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
        }
      }
    )
  }

  public ocultarMenu(){

    if(this.toggleService.estatus === ''){
      this.toggleService.estatus = 'toggled';
    }else{
      this.toggleService.estatus = '';
    }
    
  }

}
