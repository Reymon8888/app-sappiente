import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { BarraComponent } from './navegacion/barra/barra.component';
import { FooterComponent } from './footer/footer.component';
import { BienvenidaComponent } from './bienvenida/bienvenida.component';
import { LoginComponent } from './login/login.component';
import { MenuDashboardComponent } from './navegacion/menu-dashboard/menu-dashboard.component';
import { BarraDashboardComponent } from './navegacion/barra-dashboard/barra-dashboard.component';
import { TarjetasKpiComponent } from './tarjetas-kpi/tarjetas-kpi.component';
import { PaginaNoEncontradaComponent } from './pagina-no-encontrada/pagina-no-encontrada.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './services/auth.service';
import { PlantelesComponent } from './planteles/planteles.component';
import { FiltroPlantelesPipe } from './pipes/filtro-planteles.pipe';
import { CiclosComponent } from './configuraciones/ciclos/ciclos.component';
import { DepartamentosComponent } from './configuraciones/departamentos/departamentos.component';
import { EspaciosEducativosComponent } from './planteles/espacios-educativos/espacios-educativos.component';
import { TurnosComponent } from './configuraciones/turnos/turnos.component';
import { FiltroCiclosPipe } from './pipes/filtro-ciclos.pipe';
import { FiltroTurnosPipe } from './pipes/filtro-turnos.pipe';
import { SortByPipe } from './pipes/sort-by.pipe';
import { FiltroDepartamentosPipe } from './pipes/filtro-departamentos.pipe';
import { FiltroEspaciosEducativosPipe } from './pipes/filtro-espacios-educativos.pipe';
import { AsignaturasComponent } from './planes-estudio/asignaturas/asignaturas.component';
import { FiltroEspecialidadesPipe } from './pipes/filtro-especialidades.pipe';
import { FiltroAsignaturasPipe } from './pipes/filtro-asignaturas.pipe';
import { PlanesEstudioComponent } from './planes-estudio/planes-estudio.component';
import { FiltroPlanesEstudioPipe } from './pipes/filtro-planes-estudio.pipe';
import { PeriodosComponent } from './planes-estudio/periodos/periodos.component';
import { FiltroGruposPipe } from './pipes/filtro-grupos.pipe';
import { GruposAsignaturasComponent } from './planes-estudio/grupos-asignaturas/grupos-asignaturas.component';
import { PersonalComponent } from './personal/personal.component';
import { FiltroPersonalPipe } from './pipes/filtro-personal.pipe';
import { FormatoNombreDocPipe } from './pipes/formato-nombre-doc.pipe';
import { PlazasComponent } from './personal/plazas/plazas.component';
import { FiltroPlazasPipe } from './pipes/filtro-plazas.pipe';
import { AspectosEvaluacionComponent } from './profesores/aspectos-evaluacion/aspectos-evaluacion.component';
import { ClasesComponent } from './profesores/clases/clases.component';
import { ExamenesComponent } from './profesores/examenes/examenes.component';
import { ListasAlumnosComponent } from './profesores/listas-alumnos/listas-alumnos.component';
import { CalificacionesComponent } from './profesores/calificaciones/calificaciones.component';
import { GruposComponent } from './alumnos/grupos/grupos.component';
import { AspirantesComponent } from './alumnos/aspirantes/aspirantes.component';
import { AlumnosComponent } from './alumnos/alumnos/alumnos.component';
import { EgresadosComponent } from './alumnos/egresados/egresados.component';
import { ActividadesComponent } from './servicios/actividades/actividades.component';
import { NoticiasComponent } from './servicios/noticias/noticias.component';
import { IngresosComponent } from './administracion/ingresos/ingresos.component';
import { AdeudosComponent } from './administracion/adeudos/adeudos.component';
import { BecasComponent } from './administracion/becas/becas.component';
import { NotasCreditoComponent } from './administracion/notas-credito/notas-credito.component';
import { DonativosComponent } from './administracion/donativos/donativos.component';
import { FacturacionComponent } from './administracion/facturacion/facturacion.component';
import { BancosComponent } from './administracion/bancos/bancos.component';
import { FiltroNoticiasPipe } from './pipes/filtro-noticias.pipe';
import { FiltroAspectosEvaluacionPipe } from './pipes/filtro-aspectos-evaluacion.pipe';
import { FiltroClasesPipe } from './pipes/filtro-clases.pipe';
import { FiltroExamenesPipe } from './pipes/filtro-examenes.pipe';
import { FiltroPreguntasPipe } from './pipes/filtro-preguntas.pipe';
import { FiltroAlumnosPipe } from './pipes/filtro-alumnos.pipe';
import { FiltroAspirantesPipe } from './pipes/filtro-aspirantes.pipe';
import { FiltroEgresadosPipe } from './pipes/filtro-egresados.pipe';
import { TiposPreguntasComponent } from './profesores/tipos-preguntas/tipos-preguntas.component';
import { FiltroActividadesPipe } from './pipes/filtro-actividades.pipe';
import { FiltroIngresosPipe } from './pipes/filtro-ingresos.pipe';
import { FiltroAdeudosPipe } from './pipes/filtro-adeudos.pipe';
import { FiltroBecasPipe } from './pipes/filtro-becas.pipe';
import { FiltroNotasCreditoPipe } from './pipes/filtro-notas-credito.pipe';
import { FiltroFacturacionPipe } from './pipes/filtro-facturacion.pipe';
import { FiltroDonativosPipe } from './pipes/filtro-donativos.pipe';
import { FiltroBancosPipe } from './pipes/filtro-bancos.pipe';
import { InformacionAlumnoComponent } from './sitio-alummo/informacion-alumno/informacion-alumno.component';
import { ClasesAlumnoComponent } from './sitio-alummo/clases-alumno/clases-alumno.component';
import { ExamenesAlumnoComponent } from './sitio-alummo/examenes-alumno/examenes-alumno.component';
import { ActividadesAlumnoComponent } from './sitio-alummo/actividades-alumno/actividades-alumno.component';
import { HistorialAlumnoComponent } from './sitio-alummo/historial-alumno/historial-alumno.component';
import { PagosAlumnoComponent } from './sitio-alummo/pagos-alumno/pagos-alumno.component';

@NgModule({
  declarations: [
    AppComponent,
    BarraComponent,
    FooterComponent,
    BienvenidaComponent,
    LoginComponent,
    MenuDashboardComponent,
    BarraDashboardComponent,
    TarjetasKpiComponent,
    PaginaNoEncontradaComponent,
    DashboardComponent,
    PlantelesComponent,
    FiltroPlantelesPipe,
    CiclosComponent,
    DepartamentosComponent,
    EspaciosEducativosComponent,
    TurnosComponent,
    FiltroCiclosPipe,
    FiltroTurnosPipe,
    SortByPipe,
    FiltroDepartamentosPipe,
    FiltroEspaciosEducativosPipe,
    AsignaturasComponent,
    FiltroEspecialidadesPipe,
    FiltroAsignaturasPipe,
    PlanesEstudioComponent,
    FiltroPlanesEstudioPipe,
    GruposAsignaturasComponent,
    PeriodosComponent,
    FiltroGruposPipe,
    PersonalComponent,
    FiltroPersonalPipe,
    FormatoNombreDocPipe,
    PlazasComponent,
    FiltroPlazasPipe,
    AspectosEvaluacionComponent,
    ClasesComponent,
    ExamenesComponent,
    ListasAlumnosComponent,
    CalificacionesComponent,
    GruposComponent,
    AspirantesComponent,
    AlumnosComponent,
    EgresadosComponent,
    ActividadesComponent,
    NoticiasComponent,
    IngresosComponent,
    AdeudosComponent,
    BecasComponent,
    NotasCreditoComponent,
    DonativosComponent,
    FacturacionComponent,
    BancosComponent,
    FiltroNoticiasPipe,
    FiltroAspectosEvaluacionPipe,
    FiltroClasesPipe,
    FiltroExamenesPipe,
    FiltroPreguntasPipe,
    FiltroAlumnosPipe,
    FiltroAspirantesPipe,
    FiltroEgresadosPipe,
    TiposPreguntasComponent,
    FiltroActividadesPipe,
    FiltroIngresosPipe,
    FiltroAdeudosPipe,
    FiltroBecasPipe,
    FiltroNotasCreditoPipe,
    FiltroFacturacionPipe,
    FiltroDonativosPipe,
    FiltroBancosPipe,
    InformacionAlumnoComponent,
    ClasesAlumnoComponent,
    ExamenesAlumnoComponent,
    ActividadesAlumnoComponent,
    HistorialAlumnoComponent,
    PagosAlumnoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
  ],
  providers: [AuthService,{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
