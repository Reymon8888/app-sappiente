import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class AdministracionService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    public constantes: Constantes
  ) { }

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  private getAdeudos: string = this.urlBack + '/adeudos-institucion/';
  private postNuevoAdeudo: string = this.urlBack + '/adeudos/nuevo';
  private putActualizaAdeudo: string = this.urlBack + '/adeudos/actualizar/';

  private getBancos: string = this.urlBack + '/bancos-institucion/';
  private postNuevoBanco: string = this.urlBack + '/bancos/nuevo';
  private putActualizaBanco: string = this.urlBack + '/bancos/actualizar/';

  private getBecas: string = this.urlBack + '/becas-institucion/';
  private postNuevoBeca: string = this.urlBack + '/becas/nuevo';
  private putActualizaBeca: string = this.urlBack + '/becas/actualizar/';

  private getDonativos: string = this.urlBack + '/donativos-institucion/';
  private postNuevoDonativo: string = this.urlBack + '/donativos/nuevo';
  private putActualizaDonativo: string = this.urlBack + '/donativosactualizar/';

  private getFacturacion: string = this.urlBack + '/facturacion-institucion/';
  private postNuevoFacturacion: string = this.urlBack + '/facturacion/nuevo';
  private putActualizaFacturacion: string = this.urlBack + '/facturacion/actualizar/';

  private getIngresos: string = this.urlBack + '/ingresos-institucion/';
  private postNuevoIngreso: string = this.urlBack + '/ingresos/nuevo';
  private putActualizaIngreso: string = this.urlBack + '/ingresos/actualizar/';

  private getNotasCredito: string = this.urlBack + '/notas-credito-institucion/';
  private postNuevoNotaCredito: string = this.urlBack + '/notas-credito/nuevo';
  private putActualizaNotaCredito: string = this.urlBack + '/notas-credito/actualizar/';


  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneAdeudos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getAdeudos + this.idInstitucion)
  }

  public actualizaAdeudos(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaAdeudo + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaaAdeudo(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoAdeudo, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneBancos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getBancos + this.idInstitucion)
  }

  public actualizaBanco(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaBanco + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaBanco(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoBanco, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneBecas(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getBecas + this.idInstitucion)
  }

  public actualizaBecas(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaBeca + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaBeca(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoBeca, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneDonativos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getDonativos + this.idInstitucion)
  }

  public actualizaDonativo(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaDonativo + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaDonativo(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoDonativo, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneFacturacion(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getFacturacion + this.idInstitucion)
  }

  public actualizaFacturacion(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaFacturacion + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaFacturacion(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoFacturacion, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneIngresos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getIngresos + this.idInstitucion)
  }

  public actualizaIngreso(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaIngreso + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaIngreso(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoIngreso, data);
  }

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneNotasCredito(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getNotasCredito + this.idInstitucion)
  }

  public actualizaNotaCredito(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaNotaCredito + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaNotaCredito(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoNotaCredito, data);
  }


}
