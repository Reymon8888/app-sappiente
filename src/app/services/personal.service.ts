import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idUsuario = localStorage.getItem('ID_USUARIO');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  // url de servicios de personal
  private getPersonal: string = this.urlBack + '/personal-institucion/';
  private postNuevoPersonal: string = this.urlBack + '/personal/nuevo';
  private getPersonalId: string = this.urlBack + '/personal/';
  private putActualizaPersonal: string = this.urlBack + '/personal/actualizar/';
  private desactPersonal: string = this.urlBack + '/personal/desactivar/';
  private cargaFoto: string = this.urlBack + '/foto-perfil/';

  // url de servicios expedientes

  private cargaDocumento: string = this.urlBack + '/documentos/cargar-archivo';
  private buscaDocumento: string = this.urlBack + '/documentos/buscar/';
  private desactivaDocumento: string = this.urlBack + '/documentos/actualiza-archivo/';

  // url de archivos
  public filePath = this.constantes.imagenesPathServer;
  public docPath = this.constantes.documentosPathServer;

  // url de servicios plazas

  private getPlazas: string = this.urlBack + '/plazas-institucion/';
  private postNuevaPlaza: string = this.urlBack + '/plazas/nueva';
  private actualizaPlaza: string = this.urlBack + '/plazas/actualizar/'


  constructor(private httpClient: HttpClient, private router: Router, public constantes: Constantes) { }

  public obtienePersonal(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPersonal + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public obtienePersonalId(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPersonalId + this.idUsuario).pipe(tap(respuesta => {
    }));
  }

  public crearPersonal(fd){
    return this.httpClient.post<any>(this.postNuevoPersonal, fd);
  }

  public editarPersonal(fd){
    console.log('putDatos: Plantel editado -> ' + fd.id);
    this.httpClient.put<any> (this.putActualizaPersonal + Number(fd.id), fd).subscribe(
      (res) => console.log('Carga completa')
    );
  }

  public imgUpload(fd){
    console.log('try...')
    this.httpClient.put<any>(this.cargaFoto + Number(fd.id), fd).subscribe(
      (res) => console.log('Carga completa ' + res)
    );
  }

  public validaExisteDoc(persona, documento): Observable<any[]>{
    return this.httpClient.get<any[]>(this.buscaDocumento + persona + '/' + documento).pipe(tap(respuesta =>{
      console.log(respuesta+ ' documentos encontrados');
    }))
  }

  public docUpload(fdoc){
    this.httpClient.post<any>(this.cargaDocumento, fdoc).subscribe(
      (res) => { console.log('Carga completa ' + res)
     });
  }

  public eliminaDocumento(data): Observable <any> {
    return this.httpClient.put<any> (this.desactivaDocumento + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public desactivPersonal(data): Observable <any> {
    return this.httpClient.put<any> (this.desactPersonal + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }

  public obtienePlazas(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPlazas + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearPlaza(data): Observable <any> {
    return this.httpClient.post<any>(this.postNuevaPlaza, data);
  }

  public editarPlaza(data): Observable <any> {
    return this.httpClient.put<any> (this.actualizaPlaza + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }
}
