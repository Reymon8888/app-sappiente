import { TestBed } from '@angular/core/testing';

import { PlanEstudiosFullService } from './plan-estudios-full.service';

describe('PlanEstudiosFullService', () => {
  let service: PlanEstudiosFullService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlanEstudiosFullService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
