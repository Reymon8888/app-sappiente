import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class ProfesoresService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    public constantes: Constantes
    ) { }

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  // rutas de servicios

  private getAspectos: string = this.urlBack + '/aspectos-evaluacion-institucion/';
  private postNuevoAspecto: string = this.urlBack + '/aspectos-evaluacion/nuevo';
  private putActualizaAspecto: string = this.urlBack + '/aspectos-evaluacion/actualizar/';

  private getClases: string = this.urlBack + '/clases-institucion/';
  private postNuevaClase: string = this.urlBack + '/clases/nueva';
  private putActualizaClase: string = this.urlBack + '/clases/actualizar/';

  private getExamenes: string = this.urlBack + '/examenes-institucion/';
  private postNuevoExamen: string = this.urlBack + '/examenes/nuevo';
  private putActualizaExamen: string = this.urlBack + '/examenes/actualizar/';

  private getPreguntas: string = this.urlBack + '/preguntas-institucion/';
  private postNuevaPregunta: string = this.urlBack + '/preguntas/nueva';
  private putActualizaPregunta: string = this.urlBack + '/preguntas/actualizar/';

  private getTiposPreguntas: string = this.urlBack + '/tipos-preguntas-institucion/';
  private postNuevoTipoPregunta: string = this.urlBack + '/tipos-preguntas/nueva';
  private putActualizaTipoPregunta: string = this.urlBack + '/tipos-preguntas/actualizar/';

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------ASPECTOS DE EVALUACION-----------------------------
  // -----------------------------------------------------------------------------------

  public obtieneAspectos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getAspectos + this.idInstitucion)
  }

  public actualizaAspecto(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaAspecto + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaAspecto(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoAspecto, data);
  }

  // -----------------------------------------------------------------------------------
  // --------------------------------------CLASES---------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneClases(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getClases + this.idInstitucion);
  }

  public actualizaClase(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaClase + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaClase(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevaClase, data);
  }

  // -----------------------------------------------------------------------------------
  // --------------------------------------EXAMENES-------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneExamenes(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getExamenes + this.idInstitucion)
  }

  public actualizaExamen(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaExamen + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaExamen(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoExamen, data);
  }

  // -----------------------------------------------------------------------------------
  // --------------------------------------PREGUNTAS------------------------------------
  // -----------------------------------------------------------------------------------

  public obtienePreguntas(examen): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPreguntas + this.idInstitucion + '/' + examen)
  }

  public actualizaPregunta(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaPregunta + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaPregunta(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevaPregunta, data);
  }

  // -----------------------------------------------------------------------------------
  // ---------------------------------TIPOS DE PREGUNTAS------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneTiposPreguntas(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getTiposPreguntas + this.idInstitucion)
  }

  public actualizaTipoPregunta(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaTipoPregunta + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaTipoPregunta(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoTipoPregunta, data);
  }

}
