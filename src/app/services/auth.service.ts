import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UsuarioInterface } from 'src/app/models/UsuarioInterface';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject} from 'rxjs';
import { Constantes } from '../utils/constantes.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class AuthService {




  authSubject = new BehaviorSubject(false);

  public token: any;
  private urlBack = this.constantes.http;
  mensajeError: string;
  public mensaje: any;
  public idUsuario: any;
  public usuario: any;
  public correo: any;
  public idRol: any;
  public rol: any;
  public institucion: any;
  public idInstitucion: any;
  public auth: any;

  constructor(private httpClient: HttpClient, private router: Router, public constantes: Constantes){ }
  
  // método de inicio de sesión

  login(usuario): Observable<any>{

    //console.log(usuario);
    return this.httpClient.post<any>(`${this.urlBack}/acceso`,
    usuario).pipe(tap(
      usuario => {

        
        localStorage.setItem('MENSAJE', usuario.message);
        localStorage.setItem('ID_USUARIO', usuario.data.id);
        localStorage.setItem('USUARIO', usuario.data.usuario);
        localStorage.setItem('CORREO', usuario.data.correo);
        localStorage.setItem('ID_ROL', usuario.data.fk_usuario_rol);
        localStorage.setItem('ROL', usuario.data.cat_role);
        localStorage.setItem('INSTITUCION', usuario.data.cat_institucione);
        localStorage.setItem('ID_INSTITUCION', usuario.data.id_institucion);
        localStorage.setItem('AUTH', usuario.data.auth);
        localStorage.setItem('TOKEN', usuario.token);
        return usuario;
        

      }, error => {
        this.mensajeError = error.error.message;
      }));
  }


  public validaJwt(): Observable<any[]>{
    let headers = new Headers();
    headers.append('x-access-token', localStorage.getItem('TOKEN'));
    return this.httpClient.get<any[]>(`${this.constantes.http}/auth`, { headers: {'x-access-token': localStorage.getItem('TOKEN')} }).pipe(tap(respuesta => {
     
    }, error => {
        console.log('Hay un problema con el Token recibido');
        this.logout();
    }));
  }

  // método de cierre de sesión

  logout(): void{

    this.router.navigate(['bienvenida']);
    localStorage.removeItem('MENSAJE');
    localStorage.removeItem('ID_USUARIO');
    localStorage.removeItem('USUARIO');
    localStorage.removeItem('CORREO');
    localStorage.removeItem('ID_ROL');
    localStorage.removeItem('ROL');
    localStorage.removeItem('INSTITUCION');
    localStorage.removeItem('ID_INSTITUCION');
    localStorage.removeItem('AUTH');
    localStorage.removeItem('TOKEN');

  }
}

