import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CatalogosService {

  private urlBack = this.constantes.http;

  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  private getRolUsuarioUrl: string = this.urlBack + '/roles/';
  private getMenuDinamicoUrl: string = this.urlBack + '/itemsDashboard-rol/';

  // servicios para planteles

  private getPlanteles: string = this.urlBack + '/planteles-institucion/';
  private postPlantel: string = this.urlBack + '/planteles/nuevo';
  private actualizaPlantel: string = this.urlBack + '/planteles/actualizar/';

  // servicios para ciclos

  private getCiclos: string = this.urlBack + '/ciclos-institucion/';
  private postCiclos: string = this.urlBack + '/ciclos/nuevo';
  private actualizaCiclo: string = this.urlBack + '/ciclos/actualizar/';

  // servicios para turnos

  private getTurnos: string = this.urlBack + '/turnos-institucion/';
  private postTurnos: string = this.urlBack + '/turnos/nuevo';
  private actualizaTurno: string = this.urlBack + '/turnos/actualizar/';

   // servicios para departamentos

   private getDepartamentos: string = this.urlBack + '/departamentos-institucion/';
   private postDepartamento: string = this.urlBack + '/departamentos/nuevo';
   private actualizaDepartamento: string = this.urlBack + '/departamentos/actualizar/';

   // servicios para aulas

   private getEspaciosEducativos: string = this.urlBack + '/espacios-educativos-institucion/';
   private postEspacioEducativo: string = this.urlBack + '/espacios-educativos/nuevo';
   private actualizaEspacioEducativo: string = this.urlBack + '/espacios-educativos/actualizar/';

  // servicios para planes de estudio

  private getPlanesEstudio: string = this.urlBack + '/planes-estudio-institucion/';
  private postPlanEstudio: string = this.urlBack + '/planes-estudio/nuevo';
  private actualizaPlanEstudio: string = this.urlBack + '/planes-estudio/actualizar/';

   // servicios para grados

   private getPeriodos: string = this.urlBack + '/periodos-institucion/';
   private postPeriodo: string = this.urlBack + '/periodos/nuevo';
   private actualizaPeriodo: string = this.urlBack + '/periodos/actualizar/';

   // servicios para grupos

   private getGrupos: string = this.urlBack + '/grupos-asignaturas-institucion/';
   private postGrupo: string = this.urlBack + '/grupos-asignaturas/nuevo';
   private actualizaGrupo: string = this.urlBack + '/grupos-asignaturas/actualizar/';

   // servicios para roles

   private getRoles: string = this.urlBack + '/roles-institucion/';
   private postRol: string = this.urlBack + '/roles/nuevo';
   private actualizaRol: string = this.urlBack + '/roles/actualizar/';

   // servicios para asignaturas

   private getAsignaturas: string = this.urlBack + '/asignaturas-institucion/';
   private postAsignatura: string = this.urlBack + '/asignaturas/nueva';
   private actualizaAsignatura: string = this.urlBack + '/asignaturas/actualizar/';

  constructor(private httpClient: HttpClient, private router: Router, public constantes: Constantes, private authService: AuthService) { }

  public rolUsuario(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getRolUsuarioUrl + this.rolId).pipe(tap(respuesta => {
     
    }));
  }

  public menuDinamico(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getMenuDinamicoUrl + localStorage.getItem('ID_ROL')).pipe(tap(respuesta => {
    }));
  }

  // metodos para roles

  public obtieneRoles(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getRoles + this.idInstitucion).pipe(tap(respuesta => {
     // console.log(respuesta);
    }));
  }

  public crearRol(nuevoRol): Observable <any> {
    return this.httpClient.post<any>(this.postRol, nuevoRol);
  }

  public editarRol(data): Observable <any> {
    console.log('putDatos: Plantel editado -> ');
    return this.httpClient.put<any> (this.actualizaRol + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }


  // metodos para planteles

  public planteles(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPlanteles + this.idInstitucion).pipe(tap(resṕuesta => {
    }));
  }

  public crearPlantel(nuevoPlantel): Observable <any> {
    return this.httpClient.post<any>(this.postPlantel, nuevoPlantel);
  }

  public editarPlantel(data): Observable <any> {
    console.log('putDatos: Plantel editado -> ');
    return this.httpClient.put<any> (this.actualizaPlantel + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }

  // metodos para ciclos

  public obtieneCiclos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getCiclos + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearCiclo(nuevoCiclo): Observable <any> {
    return this.httpClient.post<any>(this.postCiclos, nuevoCiclo);
  }

  public editarCiclo(data): Observable <any> {
    console.log('putDatos: Proyecto editado -> ');
    return this.httpClient.put<any> (this.actualizaCiclo + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }

  // metodos para turnos

  public obtieneTurnos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getTurnos + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearTurno(nuevoTurno): Observable <any> {
    return this.httpClient.post<any>(this.postTurnos, nuevoTurno);
  }

  public editarTurno(data): Observable <any> {
   // console.log('putDatos: Turno editado -> ');
    return this.httpClient.put<any> (this.actualizaTurno + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        )
    }

    // metodos para departamentos

  public obtieneDepartamentos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getDepartamentos + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearDepartamento(nuevoDepartamento): Observable <any> {
    return this.httpClient.post<any>(this.postDepartamento, nuevoDepartamento);
  }

  public editarDepartamento(data): Observable <any> {
   // console.log('putDatos: Departamento editado -> ');
    return this.httpClient.put<any> (this.actualizaDepartamento + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }

     // metodos para espacios educativos

  public obtieneEspaciosEducativos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getEspaciosEducativos + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearEspacioEducativo(nuevoEspacio): Observable <any> {
    return this.httpClient.post<any>(this.postEspacioEducativo, nuevoEspacio);
  }

  public editarEspacioEducativo(data): Observable <any> {
 //   console.log('putDatos: Espacio editado -> ');
    return this.httpClient.put<any> (this.actualizaEspacioEducativo + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }

    // metodos para planes de estudio

  public obtienePlanesEstudio(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPlanesEstudio + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearPlanEstudio(nuevoPlan): Observable <any> {
    return this.httpClient.post<any>(this.postPlanEstudio, nuevoPlan);
  }

  public editarPlanEstudio(data): Observable <any> {
 //   console.log('putDatos: Espacio editado -> ');
    return this.httpClient.put<any> (this.actualizaPlanEstudio + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }


       // metodos para periodos

  public obtienePeriodos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPeriodos + this.idInstitucion).pipe(tap(respuesta => {
      //console.log(respuesta);
    }));
  }

  public crearPeriodo(nuevaEspecialidad): Observable <any> {
    return this.httpClient.post<any>(this.postPeriodo, nuevaEspecialidad);
  }

  public editarPeriodo(data): Observable <any> {
 //   console.log('putDatos: Espacio editado -> ');
    return this.httpClient.put<any> (this.actualizaPeriodo + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }

     // metodos para grupos de asignaturas

  public obtieneGrupos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getGrupos + this.idInstitucion).pipe(tap(respuesta => {
      //console.log(respuesta);
    }));
  }

  public crearGrupo(nuevoGrupo): Observable <any> {
    return this.httpClient.post<any>(this.postGrupo, nuevoGrupo);
  }

  public editarGrupo(data): Observable <any> {
 //   console.log('putDatos: Espacio editado -> ');
    return this.httpClient.put<any> (this.actualizaGrupo + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }

         // metodos para asignaturas
  public obtieneAsignaturas(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getAsignaturas + this.idInstitucion).pipe(tap(respuesta => {
      //console.log(respuesta);
    }));
  }

  public crearAsignatura(nuevaAsignatura): Observable <any> {
    return this.httpClient.post<any>(this.postAsignatura, nuevaAsignatura);
  }

  public editarAsignatura(data): Observable <any> {
 //   console.log('putDatos: Espacio editado -> ');
    return this.httpClient.put<any> (this.actualizaAsignatura + data.id, data).pipe(
        tap(
          (response) => {
           // console.log(data);
            }
          )
        )
    }

}
