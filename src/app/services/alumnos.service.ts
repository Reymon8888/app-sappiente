import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    public constantes: Constantes
    ) { }

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  // rutas de servicios

  private getGrupos: string = this.urlBack + '/grupos-institucion/';
  private postNuevoGrupo: string = this.urlBack + '/grupos/nuevo';
  private putActualizaGrupo: string = this.urlBack + '/grupos/actualizar/';

  private getAlumnos: string = this.urlBack + '/alumnos-institucion/';
  private postNuevoAlumno: string = this.urlBack + '/alumnos/nuevo';
  private putActualizaAlumno: string = this.urlBack + '/alumnos/actualizar/';

  // métodos

  // -----------------------------------------------------------------------------------
  // --------------------------------GRUPOS---------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneGrupos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getGrupos + this.idInstitucion)
  }

  public actualizaGrupo(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaGrupo + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaGrupo(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoGrupo, data);
  }

  // -----------------------------------------------------------------------------------
  // --------------------------------ALUMNOS--------------------------------------------
  // -----------------------------------------------------------------------------------

  public obtieneAlumnos(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getAlumnos + this.idInstitucion)
  }

  public actualizaAlumno(data): Observable <any> {
    return this.httpClient.put<any> (this.putActualizaAlumno + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public creaAlumno(data): Observable<any>{
    return this.httpClient.post<any>(this.postNuevoAlumno, data);
  }

}
