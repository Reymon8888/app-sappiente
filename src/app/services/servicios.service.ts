import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  private getNoticias: string = this.urlBack + '/noticias-institucion/';
  private getNoticiasPlantel: string = this.urlBack + '/noticias/';
  private postNuevaNoticia: string = this.urlBack + '/noticias/cargar-archivo/';
  private putNoticia: string = this.urlBack + '/noticias/actualizar/';
  private buscaFile: string = this.urlBack + '/noticias/buscar/';

  // servicios para actividades

  private getActividades: string = this.urlBack + '/actividades-institucion/';
  private postActividad: string = this.urlBack + '/actividades/nueva';
  private actualizaActividad: string = this.urlBack + '/actividades/actualizar/';

  public filePath = this.constantes.imagenesPathServer;
  public noticiaPath = this.constantes.noticiasPathServer;

  constructor(private httpClient: HttpClient, private router: Router, public constantes: Constantes) { }

  public obtieneNoticiasGeneral(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getNoticias + this.idInstitucion)
  }

  public obtieneNoticiasPlantel(data): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getNoticiasPlantel + data.institucion + '/' + data.plantel)
  }

  public eliminaNoticia(data): Observable <any> {
    return this.httpClient.put<any> (this.putNoticia + data.id, data).pipe(
      tap(
        (response) => {

        }
      )
    )
  }

  public docUpload(fd){
    this.httpClient.post<any>(this.postNuevaNoticia + this.idInstitucion, fd).subscribe(
      (res) => { console.log('Carga completa ' + res)
     });
  }

  public validaExisteDoc(noticia): Observable<any[]>{
    return this.httpClient.get<any[]>(this.buscaFile + noticia).pipe(tap(respuesta =>{
      console.log(respuesta+ ' noticias encontradas');
    }))
  }

  // metodos para actividades

  public obtieneActividades(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getActividades + this.idInstitucion).pipe(tap(respuesta => {
    }));
  }

  public crearActividad(nuevaActividad): Observable <any> {
    return this.httpClient.post<any>(this.postActividad, nuevaActividad);
  }

  public editarActividad(data): Observable <any> {
    console.log('putDatos: Proyecto editado -> ');
    return this.httpClient.put<any> (this.actualizaActividad + data.id, data).pipe(
        tap(
          (response) => {
            }
          )
        );
  }

}
