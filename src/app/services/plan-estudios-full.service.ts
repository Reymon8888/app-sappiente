import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constantes } from '../utils/constantes.service';

@Injectable({
  providedIn: 'root'
})
export class PlanEstudiosFullService {

  private urlBack = this.constantes.http;
  id = localStorage.getItem('ID_USUARIO');
  rolId = localStorage.getItem('ID_ROL');
  idInstitucion = localStorage.getItem('ID_INSTITUCION');

  private getPlanFull: string = this.urlBack + '/planes-estudio-institucion-full/';

  constructor(private httpClient: HttpClient, private router: Router, public constantes: Constantes) { }

  

  public planFull(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.getPlanFull + this.idInstitucion).pipe(tap(resṕuesta => {
      //console.log(resṕuesta + ' hgugugu')
    }));
  }
}
