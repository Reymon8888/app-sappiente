import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { PlanEstudiosFullService } from 'src/app/services/plan-estudios-full.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as moment from 'moment';
import { ExportsService } from 'src/app/services/exports.service';

@Component({
  selector: 'app-planes-estudio',
  templateUrl: './planes-estudio.component.html',
  styleUrls: ['./planes-estudio.component.css']
})
export class PlanesEstudioComponent implements OnInit {

  public formattedDate = (moment(new Date())).format('DD-MM-YYYY')
  public formattedTime = (moment(new Date())).format('HH:mm:ss')

  public logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC2BJREFUeNrsXb1y48gRHugUXGZudtlhw4tEZeeqq1oos6Ol/AIiM2cSowspho5IPQGxkTMvFToiFFzgSNjonAmbXXbwE9jT2oYL4moG84eZHhJdNaU/iMDMN939dffMgLFBBhlkkEEGGSSEJIfasb/8fDvmX0b4I3wd711S81a2fi7/8bfbegCYJpAZb98jiG1gTQRAr3j7hN8XMQOfRAzoe/zqQwDwgrf72ABPIgL1ircJbymBR9oi2FvqYCeEQQUzO+XtmgioIsl5+8CBLgaA1YAFMBeorSMWj4C/vuNA5wPAcmCnDv1m1fr+8yvXvJOwbJv7LqkAnRAAFgZ3ZQksmMeHhgHzwS0tnqVh4mdI4lILoOf8WbZHCzAf0BvUWl1TXLYYbeHBsrRZ+8hg8s34c1ZHAzCy4o2mWYQB+gCkJtRg4bNPWoxeR8Bs3x48wHyAblFrdUKSO2osFc35DYKtasbB8lz6nKCJ5wH5qJGcyHHWV4y48L5NcdKqAF2jb84PBmA0yTtF/7XFASAPrADolWI/wdXMogcYO71RNF9zqgkDA9O9UOzzRZ/ZsKTnzq6wsyQJCBEiWSLLLqMCmHdwoxDb9to5IkCrkMoaNbmMAmBFcNe8Q3N2BMLHI0OCOfINchIAXK8skphv3nWYbOcgJwHAvThkk0xtjE4cPvhNx4OXxw4uCIZGa8klz/kC1HgaALfivwFcNZCBe8hi4BTNubV84ygc+Dtv34aK9WKUX38pyh9+yqCEKcppf8f/nvLr7oMB3CIO3wkuqXj74wCuFGTgQZngkjFMArguCMD85qC5P0rIwp8DV35ggP7K24gP0r+JglyApkrYdcb//k9+3W9efTCWzWQls0sCPneCSYYN1nUpE69CQro2pp99amGaZTelllNunvfCUTzbpF9d1qYveXtkr1ekxpARM0nnmmrwRpKVgaWka4KKkmHa0JZQPqJVWLhiuqjFNYIskgXev1+AMe02kZCqGSFQKxeD1AoFd3sa5tTso0tbSi5Z+dBgmWmeEWPMlebzi8DdCKxW3oM/vpX44wwnWj8AY7ZKNGvXkdRyx1jGVPK3vIFJFg3qsqdnlFlBneVO6rloJBdPAt8LWvuWWryL7kTkJ9c4WUeC5Mx/2JddFSKu0WsNu6PMOFMt1uiw6BtJZ+cUkxlgUfhAyfojJGQdH10zeT7ZhcDnixb0LVTdg46Jvhb5uWMr/fmY0Pj5IheQqvriE0VzMZVpL1UUkDM4J26+JjTep7LxxScOtHdLFNxUl5AoytJzV2RaPLEGGOPGMZHO6ojq8lXtzzWNpXvQ4isXGhyd7+1IxtiKVW7YUO4Ev5905dhVABYN1AfC2rvo+fPHugkHS8mRuevg0w0w2viR5KZUfW92AJNon1FvTcx0lwa/F/x+S3hrybWn+yiRHIdyL7EmqSnAE82bURCfg/7OoxZvJWQr0wZ47yCxrzSYqHmGZ0493nLsuYuFpqWVarBoVlA+Ec73gGee73fvTIMl5oeyeU7ZYYtIg0ei2NxEgws2SCNeLRlazkLHep1IQo2R4CYkAcZy5pnn24ZYVCi655mOBqeEOqQCLhQVnjwz6FDu6kFHg091CRZBcFW2qvYlIaKJytpEc/mD4PefB3BfRBOV75tK1pqPdAAeU9dgXNIytdACqGPDnqmkafznt+zL0tVcgUCF5CKVYEwyVRNNgjVKwIWOmOaC56J126iR0Lb8HjABNhK//hAYYKWQ8FRTgysiCrwy/L8Xi9Vah4uPWhP4Oc/eLESXuIGQMXetSo5Pdew5hQIDlulMMlZFA27HcQpQ0N/iZKhh3xBajFRljDzJJ4FlSXUSHVTlyvD/2kXzjx2TZLLnApYsUokKYMtab6EQBrblJnA45EROI3te40SGSYHEdrPaoMH68s5S+3VDnEWrDQB7EJtyYJsJQ6xruzOhGAB2LzahyXVTUkN2DHHuGyRQJtFBFcOAReODX8vSaAqENY/8cwDQNYIMfhn87C2GX9eKVqKK5bjjE53Z6XPBd48C/vR3SGC0/TLEyLydsy/HPHSx5tCsWsRFSiuAWVzvMVLxyU8c5F3bOkC9mzfw0ZCXzgX/S3VNeG3rg1N2eALgAshP7cXsmK6EjdjnewNXEDg9aGwL8MMRAdzu2+YVoEv28nQeClktUSpZ2UTXruPQCIHeNQeC4sDljMAxFRKyWemQrPLANLhALXyjQZBgINvpyiWRA8zHvQLs6phbz3HnA5InsEyfTNgqobDoTMetnghsec0Mtkn0KZYDfGY4USgu8M90lPKkw6yR8cOWG72yluXZaoB8RwlZjNtTVwCLmPQkUP/eW7LOm5Z1umDyXPLzag6Ca8CFBEtk4U4NNBj8cBrAJ9lOLDjGsMKMFTz7xStLdp41geq5I5JJLpyISYdJeBKYhLnPA0fRPH909HFrZMRRHVKOLuZ3wZ8vRZOyK5NltKucmHnel+ddEJiLnkSE8cTA2nYCLMq5jj0XHlwzd9CGKfvydpNdJEUU2WkLtRHAmMER+VovRyV0MEdXk+cRNZok0DgGRqctqBQbRFo88ZT08DXoUwQaNHoaMKGjo0x111FWKgDnXaHHgQDc1mhY7P5VzTgguZpqciR1gDGkEH3QNbGZvj8xL7DVFlr91NOZlzqkcGSaiFGtB98F1OISWaJOma7GWm7Fut/6+SJhIGCkK9zCEkJ7ReZZqS6tBDBmdMoQWgzxHW+6WpjjM7XBFR0m1u5Xhfe6fOV+0wBhlUx7lSa8zooOmRb7WDesczwDEMPmBRo5JgLe4FKc2d4EOt8fLEwaXLQmwBwBX3nU3rRDewunACNbE2nxjYcQQ/XzG1K4RFBn7SwP9mPWThDg0fxv2xFD6w0oNWbt4O+FRy1e2WoviO6y2TkTvwNhxRy8eMoBwEskhqVssnKgQCOv9shk/spkqfHv8HXm4xDSjrfKFTpFkMTg5jtJZqm3HDW/739VtBfJVbTS8fITkHOdRX8mOxukr3wJmA2qWcTbPFsie6tcrruiUxtgNGVLCeHaBIqNl7HsNpBo71Rimmtm8H4Mo71JSEpKia/sg20WHaZ5HTm4XeNm9FY5m81nMlM97SH7c4kkbj9GPRS/u2PyF34aLUJILB/sRmHW5T3FiCmGMOWBgCtbDntuukAhcfCAH5m8GH0eOwg9A/zYEQJajZ+L/cFNzlckuwPZldgHuJsOcGe2ypE4etBxhw95Xsk4aPILswyubdp3TJ84fOiMdb8Re3aE7znU9bkNqbp0cT9nRzhg+qxrxm0C11ZDg5sqgFsyh29RT3roBJidrtopaPE8tqWrDixcV226RFfmbFy+cd2RX38pyh9+yj53MGuYwX/i1/2LX//bEYB7i5P+W5/g9qLBmpr8nD+OPQvVYZJhDLKOS3sBt1eANcwSSIEErDogcIFrLBT63qu7Sjx0dIwgpwqX//+Io8h97Yqp1a/XfW8qTzx1ulkflSlcXqHZziMDNkVgVVZ81Ki1vfcx8TwIt0x9/VaFGr2lrNFooa6Z+usFSuYgQ0USYAOT3cx2mOl3lHw0ksgrprdvau37nI8k0OA0KzF1kx4w62F1ZxECbFwr9R7N8EjzuechNpQngbWgKXJnBv8Og3bPNBehGfhVeLZ3BqA21ucOF0gEkaAA72nGitntIizQb3/G75VrxWhRmlfTpgjomNkd3dhr+BMVwHt+bcHcbxet2ddLjFLWz7bUnBFaH0YKYEsCE1Jq5AY5tWRNQnnU0EdfYQhCcRcjuIIPlGP2JBINsWGwfYB6j/F5RX3cogH4Fc1u2G3WM+DN9tUHZOxRpVGjBFgQzoyxfd8iUKmmZjIE8pmUUX0Z9tEBrDgJ9glbdUjVq0EGGWSQQQYZhJb8T4ABAAV05c/Eo4lWAAAAAElFTkSuQmCC';

  public institucion: string;
  public idInstitucion = Number(localStorage.getItem('ID_INSTITUCION'));

  public nombreUsuario: any;
  public idUsuario: number;
  public listaPlanesEstudio = new Array<any>();
  public listaPlanFull = new Array<any>();
  public listaPlanteles = new Array<any>();
  public plantel = "";
  public idItemSeleccionado: number;
  public registroSeleccionado = new Array<any>();
  public planEstudioSeleccionado: any;
  public rolAdm:any;
  public filtroPlanEstudio = '';

  public formPlanEstudio: FormGroup;
  public validador = '';
  public edicion: boolean;

  public itemsPerPage: number = 10; // numero de registros por pagina
  public showDatos: boolean = false;
  public paginadorshow: boolean = false;
  public p;

  public confirmaGuardado: boolean;
  public confirmaEdicion: boolean;

  public activo = '';


  constructor(
    private authService: AuthService,
    private catService: CatalogosService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private exportExcel: ExportsService,
    private planEstudioFull: PlanEstudiosFullService) { }


  logOut(): void{
      this.authService.logout();
  }

  ngOnInit(): void {
    

    //window.location.reload();

    this.edicion = false;
    this.catService.rolUsuario();
    this.idUsuario = Number(localStorage.getItem('ID_USUARIO'));
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.institucion = localStorage.getItem('INSTITUCION');
    this.rolAdm = localStorage.getItem('ROL');
    this.getPlanesEstudio();
    this.getPlanteles();
    this.planFull();
    this.formPlanEstudio = this.formBuilder.group({
    clave: new FormControl(null, Validators.required),
    planEstudio: new FormControl(null, Validators.required),
    descripcion: new FormControl(null, Validators.required),
    plantel: new FormControl(null, Validators.required)
    });
  }

  // setea formulario para nuevo registro

  public nuevo(){
    this.edicion = false;
    this.formPlanEstudio.reset();
    this.validador = '';
    this.activo = 'true';
    this.formPlanEstudio.controls.plantel.setValue('');
  }

  // lista de planes de estudio completa
  planFull(){
    this.spinner.show();
    this.planEstudioFull.planFull().subscribe(
      items => {
        this.listaPlanFull = items;
       //console.log(items);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // lista de planteles
  getPlanteles(){
    this.spinner.show();
    this.catService.planteles().subscribe(
      items => {
        this.listaPlanteles = items;
        //console.log(this.listaPlanteles.length + ' registros');
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // lista de planes de estudio

  getPlanesEstudio(){
    this.spinner.show();
    this.catService.obtienePlanesEstudio().subscribe(
      items => {
        this.listaPlanesEstudio = items;
        //console.log(items);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // guarda plan de estudio

  public guardarNuevoPlanEstudio(): void {
    this.spinner.show();
    this.validador = '';
    if(this.formPlanEstudio.valid) {

    this.catService.crearPlanEstudio(
      {
        clave: this.formPlanEstudio.controls.clave.value,
        planEstudio: this.formPlanEstudio.controls.planEstudio.value,
        descripcion: this.formPlanEstudio.controls.descripcion.value,
        institucion: this.idInstitucion,
        plantel: this.formPlanEstudio.controls.plantel.value,
        registradoPor: this.idUsuario
      })
    .subscribe(response => {
      window.scroll(0, 0);
      this.formPlanEstudio.reset();
      this.getPlanesEstudio();
      //console.log(response);
      this.confirmaGuardado = true;
      this.spinner.hide();

      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.confirmaGuardado = false;
      }, 2500);
    });
    }
    else {
      this.validador = 'Favor de llenar los campos obligatorios';
     // console.log(this.validador)
      this.spinner.hide();
    }
  }

  // funcion para editar registro
  public updatePlanEstudio(id) {
    this.spinner.show();
    this.validador = '';
    this.edicion = true;

    //console.log(id);
    this.idItemSeleccionado = id;
    this.registroSeleccionado = this.listaPlanesEstudio.filter(id => id.id === this.idItemSeleccionado);

    this.planEstudioSeleccionado = this.registroSeleccionado.values;

    for (const planEstudioSeleccionado of this.registroSeleccionado) {
      //console.log(plantelSeleccionado);
      //console.log(plantelSeleccionado.descripcion);
      this.formPlanEstudio.controls.clave.setValue(planEstudioSeleccionado.clave);
      this.formPlanEstudio.controls.planEstudio.setValue(planEstudioSeleccionado.plan_estudio);
      this.formPlanEstudio.controls.descripcion.setValue(planEstudioSeleccionado.descripcion);
      this.formPlanEstudio.controls.plantel.setValue(planEstudioSeleccionado.cat_plantele.id);

      this.idItemSeleccionado = planEstudioSeleccionado.id;
    }
    //console.log(this.registroSeleccionado);
    this.spinner.hide();
  }

  // función para guardar cambios
  public guardarCambios(): void {
    this.spinner.show();
    //console.log(this.formPlantel.value);


    if (this.formPlanEstudio.valid) { // Recibe objeto de formulario y valida
    //console.log(this.idItemSeleccionado);
    this.catService.editarPlanEstudio( // Envía método post al servicio con los valores del formulario
      {
        id: this.idItemSeleccionado,
        clave: this.formPlanEstudio.controls.clave.value,
        planEstudio: this.formPlanEstudio.controls.planEstudio.value,
        descripcion: this.formPlanEstudio.controls.descripcion.value,
        plantel: this.formPlanEstudio.controls.plantel.value,
        registradoPor: this.idUsuario
      }
    )
    .subscribe(response => { // Se completa el registro
      this.formPlanEstudio.reset(); // Limpia campos del formulario
     // console.log('bla v+ssasa' + this.formPlantel);
      this.confirmaEdicion = true;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.confirmaEdicion = false;
        this.edicion = false;
      }, 2500);
      this.getPlanesEstudio();
      this.spinner.hide();
    });
    } else {
      this.validador = 'Favor de llenar todos los campos'; // Si hay campos vacíos muestra alerta
      this.spinner.hide();
    }
  }

  // función para desactivar registro
  public eliminarPlanEstudio(id): void {
    this.spinner.show();

    this.idItemSeleccionado = id;
    //console.log(this.idItemSeleccionado);
    this.catService.editarPlanEstudio( // Envía método post al servicio con los valores del formulario
      {
        id: this.idItemSeleccionado,
        estatus: 0,
        registradoPor: this.idUsuario
      }
    )
    .subscribe(response => { // Se completa el registro
      this.formPlanEstudio.reset(); // Limpia campos del formulario
      //console.log('bla v+ssasa' + this.formPlantel);
      this.getPlanesEstudio();
      this.p = 1;
      this.spinner.hide();
    });
  }

  // funcion para imprimir PDF

  public descargarPdf() {

    var doc = new jsPDF('p', 'mm', 'A4')
    // cabeceras del reporte

    const addHeaders = doc => {
      const pageCount = doc.internal.getNumberOfPages()

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {

        doc.setPage(i)
        autoTable(doc, {
          theme: 'grid',
          margin: { top: 20 , bottom:50},
          head: [[{ content: 'SAPP - Reporte de planes de estudio', colSpan: 4, styles: { halign: 'center', minCellHeight: 14, fillColor: '#fff', fontSize: 18, textColor: '#000' } }]],
          body: [
            [{ content: 'Instituto: ', styles: { fontStyle: 'bold' } }, this.institucion, { content: 'Fecha: ', styles: { fontStyle: 'bold' } }, this.formattedDate],
            [{ content: 'Usuario: ', styles: { fontStyle: 'bold' } }, this.nombreUsuario, { content: 'Hora: ', styles: { fontStyle: 'bold' } }, this.formattedTime],
            // ...
          ],
        })
        doc.text('Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 287, {
          align: 'center'
        })
        
        doc.addImage(this.logo,'png',180, 3,20,20)
      }
    }

      // datos del reporte

      addHeaders(doc)
      autoTable(doc, { theme: 'grid',html: '#reportePdf', margin: { top: 50 }, headStyles: { fillColor: '#527780' } })
      const newHead = doc => {
        const pageCount = doc.internal.getNumberOfPages()
        if(pageCount > 1) {
          addHeaders(doc)
        }
      }
      newHead(doc)
      doc.save('PlanesEstudio.pdf');

  }

    // funcion para imprimir plan completo PDF

    public planFullPdf(idPlanFull) {
      const idPlan = idPlanFull
      var doc = new jsPDF('p', 'mm', 'A4')
      // cabeceras del reporte
  
      const addHeaders = doc => {
        const pageCount = doc.internal.getNumberOfPages()
  
        doc.setFont('helvetica', 'italic')
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
  
          doc.setPage(i)
          const title = this.listaPlanFull.filter(titl => titl.id === idPlan)
          title.forEach(its => {

            autoTable(doc, {
              theme: 'grid',
              margin:{top:20},
              head: [[{ content: its.plan_estudio, colSpan: 4, styles: { halign: 'center', minCellHeight: 14, fillColor: '#fff', fontSize: 20, textColor: '#000' } }]],
              body: [
                [{ content: 'Instituto: ', styles: { fontStyle: 'bold' } }, this.institucion, { content: 'Fecha: ', styles: { fontStyle: 'bold' } }, this.formattedDate],
                [{ content: 'Usuario: ', styles: { fontStyle: 'bold' } }, this.nombreUsuario, { content: 'Hora: ', styles: { fontStyle: 'bold' } }, this.formattedTime],
                // ...
              ],
            })
            doc.text('Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 287, {
              align: 'center'
            })

            
          })
          
          for(const periodo of title){
            //console.log(periodo.cat_periodos_escolares)
            for(const per of periodo.cat_periodos_escolares){
              autoTable(doc,{
                theme: 'grid',
                margin: {top: 90},
                head:[[{ content: per.periodo, colSpan: 3, styles: { halign: 'center', minCellHeight: 8, fillColor: '#527780', fontSize: 12, textColor: '#ccc'}}]],
              })
              for(const grup of per.cat_grupo_asignaturas){
                autoTable(doc,{
                  theme: 'plain',
                  body:[[{ content: grup.grupo, colSpan: 3, styles: { halign: 'left', minCellHeight: 6, fillColor: '#fff', fontSize: 10, textColor: '#000', fontStyle: 'bold'}}]]
                })
                for(const mat of grup.cat_materias){
                  //console.log(mat.asignatura)
                  autoTable(doc,{
                    theme: 'striped',
                    body:[[{ content: mat.asignatura, styles: { halign: 'left', minCellHeight: 4, fillColor: '#fff', fontSize: 8, textColor: '#000'}}]]
                  })
                }
              }
            }
          }
          
          doc.addImage(this.logo,'png',180, 3,20,20)
        }
      }

      // datos del reporte

     

  
      addHeaders(doc)
        
        const newHead = doc => {
          const pageCount = doc.internal.getNumberOfPages()
          if(pageCount > 1) {
            addHeaders(doc)
          }
        }
        newHead(doc)
        doc.save('PlanEstudioCompleto.pdf');
  
    }

  // exporta excel

  public exportaExcel(): void{
    this.exportExcel.exportToExcel(this.listaPlanesEstudio, 'my_export');
  }
}
