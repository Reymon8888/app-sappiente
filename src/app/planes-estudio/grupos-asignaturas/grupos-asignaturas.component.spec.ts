import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposAsignaturasComponent } from './grupos-asignaturas.component';

describe('GruposAsignaturasComponent', () => {
  let component: GruposAsignaturasComponent;
  let fixture: ComponentFixture<GruposAsignaturasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposAsignaturasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposAsignaturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
