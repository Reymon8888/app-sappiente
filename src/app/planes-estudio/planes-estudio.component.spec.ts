import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanesEstudioComponent } from './planes-estudio.component';

describe('PlanesEstudioComponent', () => {
  let component: PlanesEstudioComponent;
  let fixture: ComponentFixture<PlanesEstudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanesEstudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanesEstudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
