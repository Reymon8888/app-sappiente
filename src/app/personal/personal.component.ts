import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogosService} from 'src/app/services/catalogos.service';
import { PersonalService } from 'src/app/services/personal.service';
import { FormControl, FormGroup, Validators, FormBuilder, DefaultValueAccessor } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute} from '@angular/router';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as moment from 'moment';
import { ExportsService } from 'src/app/services/exports.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  // variables de carga de imagen de perfil, documentos y de sesión

  public logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAC2BJREFUeNrsXb1y48gRHugUXGZudtlhw4tEZeeqq1oos6Ol/AIiM2cSowspho5IPQGxkTMvFToiFFzgSNjonAmbXXbwE9jT2oYL4moG84eZHhJdNaU/iMDMN939dffMgLFBBhlkkEEGGSSEJIfasb/8fDvmX0b4I3wd711S81a2fi7/8bfbegCYJpAZb98jiG1gTQRAr3j7hN8XMQOfRAzoe/zqQwDwgrf72ABPIgL1ircJbymBR9oi2FvqYCeEQQUzO+XtmgioIsl5+8CBLgaA1YAFMBeorSMWj4C/vuNA5wPAcmCnDv1m1fr+8yvXvJOwbJv7LqkAnRAAFgZ3ZQksmMeHhgHzwS0tnqVh4mdI4lILoOf8WbZHCzAf0BvUWl1TXLYYbeHBsrRZ+8hg8s34c1ZHAzCy4o2mWYQB+gCkJtRg4bNPWoxeR8Bs3x48wHyAblFrdUKSO2osFc35DYKtasbB8lz6nKCJ5wH5qJGcyHHWV4y48L5NcdKqAF2jb84PBmA0yTtF/7XFASAPrADolWI/wdXMogcYO71RNF9zqgkDA9O9UOzzRZ/ZsKTnzq6wsyQJCBEiWSLLLqMCmHdwoxDb9to5IkCrkMoaNbmMAmBFcNe8Q3N2BMLHI0OCOfINchIAXK8skphv3nWYbOcgJwHAvThkk0xtjE4cPvhNx4OXxw4uCIZGa8klz/kC1HgaALfivwFcNZCBe8hi4BTNubV84ygc+Dtv34aK9WKUX38pyh9+yqCEKcppf8f/nvLr7oMB3CIO3wkuqXj74wCuFGTgQZngkjFMArguCMD85qC5P0rIwp8DV35ggP7K24gP0r+JglyApkrYdcb//k9+3W9efTCWzWQls0sCPneCSYYN1nUpE69CQro2pp99amGaZTelllNunvfCUTzbpF9d1qYveXtkr1ekxpARM0nnmmrwRpKVgaWka4KKkmHa0JZQPqJVWLhiuqjFNYIskgXev1+AMe02kZCqGSFQKxeD1AoFd3sa5tTso0tbSi5Z+dBgmWmeEWPMlebzi8DdCKxW3oM/vpX44wwnWj8AY7ZKNGvXkdRyx1jGVPK3vIFJFg3qsqdnlFlBneVO6rloJBdPAt8LWvuWWryL7kTkJ9c4WUeC5Mx/2JddFSKu0WsNu6PMOFMt1uiw6BtJZ+cUkxlgUfhAyfojJGQdH10zeT7ZhcDnixb0LVTdg46Jvhb5uWMr/fmY0Pj5IheQqvriE0VzMZVpL1UUkDM4J26+JjTep7LxxScOtHdLFNxUl5AoytJzV2RaPLEGGOPGMZHO6ojq8lXtzzWNpXvQ4isXGhyd7+1IxtiKVW7YUO4Ev5905dhVABYN1AfC2rvo+fPHugkHS8mRuevg0w0w2viR5KZUfW92AJNon1FvTcx0lwa/F/x+S3hrybWn+yiRHIdyL7EmqSnAE82bURCfg/7OoxZvJWQr0wZ47yCxrzSYqHmGZ0493nLsuYuFpqWVarBoVlA+Ec73gGee73fvTIMl5oeyeU7ZYYtIg0ei2NxEgws2SCNeLRlazkLHep1IQo2R4CYkAcZy5pnn24ZYVCi655mOBqeEOqQCLhQVnjwz6FDu6kFHg091CRZBcFW2qvYlIaKJytpEc/mD4PefB3BfRBOV75tK1pqPdAAeU9dgXNIytdACqGPDnqmkafznt+zL0tVcgUCF5CKVYEwyVRNNgjVKwIWOmOaC56J126iR0Lb8HjABNhK//hAYYKWQ8FRTgysiCrwy/L8Xi9Vah4uPWhP4Oc/eLESXuIGQMXetSo5Pdew5hQIDlulMMlZFA27HcQpQ0N/iZKhh3xBajFRljDzJJ4FlSXUSHVTlyvD/2kXzjx2TZLLnApYsUokKYMtab6EQBrblJnA45EROI3te40SGSYHEdrPaoMH68s5S+3VDnEWrDQB7EJtyYJsJQ6xruzOhGAB2LzahyXVTUkN2DHHuGyRQJtFBFcOAReODX8vSaAqENY/8cwDQNYIMfhn87C2GX9eKVqKK5bjjE53Z6XPBd48C/vR3SGC0/TLEyLydsy/HPHSx5tCsWsRFSiuAWVzvMVLxyU8c5F3bOkC9mzfw0ZCXzgX/S3VNeG3rg1N2eALgAshP7cXsmK6EjdjnewNXEDg9aGwL8MMRAdzu2+YVoEv28nQeClktUSpZ2UTXruPQCIHeNQeC4sDljMAxFRKyWemQrPLANLhALXyjQZBgINvpyiWRA8zHvQLs6phbz3HnA5InsEyfTNgqobDoTMetnghsec0Mtkn0KZYDfGY4USgu8M90lPKkw6yR8cOWG72yluXZaoB8RwlZjNtTVwCLmPQkUP/eW7LOm5Z1umDyXPLzag6Ca8CFBEtk4U4NNBj8cBrAJ9lOLDjGsMKMFTz7xStLdp41geq5I5JJLpyISYdJeBKYhLnPA0fRPH909HFrZMRRHVKOLuZ3wZ8vRZOyK5NltKucmHnel+ddEJiLnkSE8cTA2nYCLMq5jj0XHlwzd9CGKfvydpNdJEUU2WkLtRHAmMER+VovRyV0MEdXk+cRNZok0DgGRqctqBQbRFo88ZT08DXoUwQaNHoaMKGjo0x111FWKgDnXaHHgQDc1mhY7P5VzTgguZpqciR1gDGkEH3QNbGZvj8xL7DVFlr91NOZlzqkcGSaiFGtB98F1OISWaJOma7GWm7Fut/6+SJhIGCkK9zCEkJ7ReZZqS6tBDBmdMoQWgzxHW+6WpjjM7XBFR0m1u5Xhfe6fOV+0wBhlUx7lSa8zooOmRb7WDesczwDEMPmBRo5JgLe4FKc2d4EOt8fLEwaXLQmwBwBX3nU3rRDewunACNbE2nxjYcQQ/XzG1K4RFBn7SwP9mPWThDg0fxv2xFD6w0oNWbt4O+FRy1e2WoviO6y2TkTvwNhxRy8eMoBwEskhqVssnKgQCOv9shk/spkqfHv8HXm4xDSjrfKFTpFkMTg5jtJZqm3HDW/739VtBfJVbTS8fITkHOdRX8mOxukr3wJmA2qWcTbPFsie6tcrruiUxtgNGVLCeHaBIqNl7HsNpBo71Rimmtm8H4Mo71JSEpKia/sg20WHaZ5HTm4XeNm9FY5m81nMlM97SH7c4kkbj9GPRS/u2PyF34aLUJILB/sRmHW5T3FiCmGMOWBgCtbDntuukAhcfCAH5m8GH0eOwg9A/zYEQJajZ+L/cFNzlckuwPZldgHuJsOcGe2ypE4etBxhw95Xsk4aPILswyubdp3TJ84fOiMdb8Re3aE7znU9bkNqbp0cT9nRzhg+qxrxm0C11ZDg5sqgFsyh29RT3roBJidrtopaPE8tqWrDixcV226RFfmbFy+cd2RX38pyh9+yj53MGuYwX/i1/2LX//bEYB7i5P+W5/g9qLBmpr8nD+OPQvVYZJhDLKOS3sBt1eANcwSSIEErDogcIFrLBT63qu7Sjx0dIwgpwqX//+Io8h97Yqp1a/XfW8qTzx1ulkflSlcXqHZziMDNkVgVVZ81Ki1vfcx8TwIt0x9/VaFGr2lrNFooa6Z+usFSuYgQ0USYAOT3cx2mOl3lHw0ksgrprdvau37nI8k0OA0KzF1kx4w62F1ZxECbFwr9R7N8EjzuechNpQngbWgKXJnBv8Og3bPNBehGfhVeLZ3BqA21ucOF0gEkaAA72nGitntIizQb3/G75VrxWhRmlfTpgjomNkd3dhr+BMVwHt+bcHcbxet2ddLjFLWz7bUnBFaH0YKYEsCE1Jq5AY5tWRNQnnU0EdfYQhCcRcjuIIPlGP2JBINsWGwfYB6j/F5RX3cogH4Fc1u2G3WM+DN9tUHZOxRpVGjBFgQzoyxfd8iUKmmZjIE8pmUUX0Z9tEBrDgJ9glbdUjVq0EGGWSQQQYZhJb8T4ABAAV05c/Eo4lWAAAAAElFTkSuQmCC';

  public formattedDate = (moment(new Date())).format('DD-MM-YYYY');
  public formattedTime = (moment(new Date())).format('HH:mm:ss');
  public imgPath = this.personalService.filePath;
  public docPath = this.personalService.docPath;
  public imagen: '';
  public nombreDocumento: any;
  public imgURL = '';
  public alertaSize = '';
  public tipoDoc: string = '';
  public documento = '';
  public idAsignado = '';
  public documentosEcontrados:any;

  public nombreUsuario: any;
  public idUsuario: number;

  public institucion: string;
  public idInstitucion = Number(localStorage.getItem('ID_INSTITUCION'));
  public inst: any = localStorage.getItem('ID_INSTITUCION');

  // variables de crud personal

  
  public usu: any;
  public listaPersonal = new Array<any>();
  public listaDepartamentos = new Array<any>();
  public listaRoles = new Array<any>();
  public plantel = "";
  public idItemSeleccionado: number;
  public registroSeleccionado = new Array<any>();
  public personalSeleccionado: any;
  public rolAdm:any;
  public filtroPersonal = '';

  // varaibles para formulario

  public formPersonal: FormGroup;
  public formDocument: FormGroup;
  public validador = '';
  public edicion: boolean;
  public confirmaGuardado: boolean;
  public confirmaEdicion: boolean;

  // variables de paginador|

  public itemsPerPage: number = 10; // numero de registros por pagina
  public showDatos: boolean = false;
  public paginadorshow: boolean = false;
  public p;

  // variables de stepper

  public valueBarra: number;
  public paso1 = true;
  public paso2 = false;
  public paso3 = false;
  public paso4 = false;
  public paso5 = false;
  public activo = '';

  constructor(
    private authService: AuthService,
    private catService: CatalogosService,
    private personalService: PersonalService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private router: Router,
    private exportExcel: ExportsService) { }


  logOut(): void{
      this.authService.logout();
  }

  ngOnInit(): void {
    

    //window.location.reload();

    this.nombreDocumento = '';
    this.edicion = false;
    this.valueBarra = 0;
    this.catService.rolUsuario();
    this.idUsuario = Number(localStorage.getItem('ID_USUARIO'));
    this.usu = localStorage.getItem('ID_USUARIO');
    this.nombreUsuario = localStorage.getItem('USUARIO');
    this.institucion = localStorage.getItem('INSTITUCION');
    this.rolAdm = localStorage.getItem('ROL');
    
    this.getDepartamentos();
    this.getPersonal();
    this.getRoles();
    this.formPersonal = this.formBuilder.group({
    clave: new FormControl(null, Validators.required),
    rol: new FormControl(null, Validators.required),
    nombres: new FormControl(null, Validators.required),
    apellidoPat: new FormControl(null, Validators.required),
    apellidoMat: new FormControl(null, Validators.required),
    genero: new FormControl(null, Validators.required),
    fechaNacimiento: new FormControl(null, Validators.required),
    nacionalidad: new FormControl(null, Validators.required),
    curp: new FormControl(null, Validators.required),
    rfc: new FormControl(null, Validators.required),
    nss: new FormControl(null, Validators.required),
    estadoCivil: new FormControl(null, Validators.required),
    nombrePadre: new FormControl(null, Validators.required),
    nombreMadre: new FormControl(null, Validators.required),
    domicilio: new FormControl(null, Validators.required),
    cp: new FormControl(null, Validators.required),
    colonia: new FormControl(null, Validators.required),
    ciudad: new FormControl(null, Validators.required),
    municipio: new FormControl(null, Validators.required),
    entidad: new FormControl(null, Validators.required),
    pais: new FormControl(null, Validators.required),
    telefono: new FormControl(null, Validators.required),
    movil: new FormControl(null),
    correo: new FormControl(null, Validators.required),
    socialNet: new FormControl(null, Validators.required),
    departamento: new FormControl(null, Validators.required),
    contrasena: new FormControl(null, Validators.required),
    });
    this.formDocument = this.formBuilder.group({
      tipo: new FormControl(null, Validators.required)
    })
    this.formDocument.controls.tipo.setValue('');
    this.stepper1();
  }

  // stepper

  public stepper1(){
    this.paso1 = true;
    this.paso2 = false;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''

  }

  public stepper2(){

    if(
      this.formPersonal.controls.clave.value == '' ||
      this.formPersonal.controls.rol.value == '' ||
      this.formPersonal.controls.nombres.value == '' ||
      this.formPersonal.controls.apellidoPat.value == '' ||
      this.formPersonal.controls.apellidoMat.value == '' ||
      this.formPersonal.controls.genero.value == '' ||
      this.formPersonal.controls.fechaNacimiento.value == '' ||
      this.formPersonal.controls.clave.value == null ||
      this.formPersonal.controls.rol.value == null ||
      this.formPersonal.controls.nombres.value == null ||
      this.formPersonal.controls.apellidoPat.value == null ||
      this.formPersonal.controls.apellidoMat.value == null ||
      this.formPersonal.controls.genero.value == null ||
      this.formPersonal.controls.fechaNacimiento.value == null ){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
    this.paso2 = true;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''
    }
  }

  public stepper3(){

    if(
      this.formPersonal.controls.nacionalidad.value == '' ||
      this.formPersonal.controls.curp.value == '' ||
      this.formPersonal.controls.rfc.value == '' ||
      this.formPersonal.controls.nss.value == '' ||
      this.formPersonal.controls.estadoCivil.value == '' ||
      this.formPersonal.controls.nombrePadre.value == '' ||
      this.formPersonal.controls.nombreMadre.value == '' ||
      this.formPersonal.controls.nacionalidad.value == null ||
      this.formPersonal.controls.curp.value == null ||
      this.formPersonal.controls.rfc.value == null ||
      this.formPersonal.controls.nss.value == null ||
      this.formPersonal.controls.estadoCivil.value == null ||
      this.formPersonal.controls.nombrePadre.value == null ||
      this.formPersonal.controls.nombreMadre.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
    this.paso1 = false;
    this.paso2 = false;
    this.paso3 = true;
    this.paso4 = false;
    this.paso5 = false;
    this.validador = ''
    }

  }

  public stepper4(){

    if(
      this.formPersonal.controls.domicilio.value == '' ||
      this.formPersonal.controls.cp.value == '' ||
      this.formPersonal.controls.colonia.value == '' ||
      this.formPersonal.controls.municipio.value == '' ||
      this.formPersonal.controls.ciudad.value == '' ||
      this.formPersonal.controls.entidad.value == '' ||
      this.formPersonal.controls.pais.value == '' ||
      this.formPersonal.controls.domicilio.value == null ||
      this.formPersonal.controls.cp.value == null ||
      this.formPersonal.controls.colonia.value == null ||
      this.formPersonal.controls.municipio.value == null ||
      this.formPersonal.controls.ciudad.value == null ||
      this.formPersonal.controls.entidad.value == null ||
      this.formPersonal.controls.pais.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
      this.paso2 = false;
      this.paso3 = false;
      this.paso4 = true;
      this.paso5 = false;
    this.validador = ''
    }
  }

  public stepper5(){

    if(
      this.formPersonal.controls.telefono.value == '' ||
      this.formPersonal.controls.movil.value == '' ||
      this.formPersonal.controls.correo.value == '' ||
      this.formPersonal.controls.socialNet.value == '' ||
      this.formPersonal.controls.telefono.value == null ||
      this.formPersonal.controls.movil.value == null ||
      this.formPersonal.controls.correo.value == null ||
      this.formPersonal.controls.socialNet.value == null){
      this.validador = 'Favor de llenar los campos obligatorios'
    }else{
      this.paso1 = false;
    this.paso2 = false;
    this.paso3 = false;
    this.paso4 = false;
    this.paso5 = true;
    this.validador = ''
    }
  }


  public cargaImagen(event) {
    this.alertaSize = '';
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      var nameFile = event.target.files[0].name;
      var ext = nameFile.split('.').pop();
      ext = ext.toLowerCase();
      const reader = new FileReader();

      this.imagen = file;
      if (ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif' && ext !== 'png') {
        this.alertaSize = 'Formato de archivo no permitido';
        this.imgURL = '';
        this.imagen = '';
      } else if (file.size > 2000000) {
        this.alertaSize = 'El tamaño máximo del archivo debe ser de 2MB';
        this.imgURL = '';
        this.imagen = '';
      } else {
        reader.readAsDataURL(file);
        reader.onload = (event: any) => {
          this.imgURL = event.target.result;
          console.log(ext);
        }
      }
    }

  }

  public cargaDocumento(event) {
    this.validador = '';
    this.alertaSize = '';
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      var nameFile = event.target.files[0].name;
      
      var ext = nameFile.split('.').pop();
      ext = ext.toLowerCase();
      const reader = new FileReader();

      this.documento = file;
      if (ext !== 'jpg' && ext !== 'jpeg' && ext !== 'png' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx') {
        this.alertaSize = 'Formato de archivo no permitido';
        this.imgURL = '';
        this.nombreDocumento = '';
      } else if (file.size > 4000000) {
        this.alertaSize = 'El tamaño máximo del archivo debe ser de 4MB';
        this.imgURL = '';
        this.nombreDocumento = '';
      } else {
        reader.readAsDataURL(file);
        reader.onload = (event: any) => {
          this.imgURL = event.target.result;
          this.nombreDocumento = nameFile;
          //console.log(file);
        }
      }
    }

  }

  public subirImg(iden){
    this.spinner.show();
    const fd = new FormData();
    fd.append('id', iden);
    fd.append('file', this.imagen);
    fd.append('foto', this.imagen['name'])
    fd.append('registradoPor', String(this.idUsuario));
    this.personalService.imgUpload(
      fd
    );
    setTimeout(() => {
      this.imagen = '';
      this.imgURL = '';
      this.getPersonal();
      this.imgURL = '';
      this.spinner.hide();
    }, 2500);
  }

  public setearDocumento(){
    this.formDocument.controls.tipo.setValue('');
    this.nombreDocumento = '';
    this.validador = '';
    this.documento = '';
  }


  public subirDoc(pers){
    this.spinner.show();
    console.log('okokokok')
    const fdoc = new FormData();
    fdoc.append('personal', pers);
    fdoc.append('file', this.documento);
    fdoc.append('documento', this.documento['name']);
    fdoc.append('tipo', this.formDocument.controls.tipo.value);
    fdoc.append('institucion', String(this.idInstitucion));
    fdoc.append('registradoPor', String(this.idUsuario));
    if(this.formDocument.controls.tipo.value !== '')
    {
      this.personalService.validaExisteDoc(pers, this.documento['name']).subscribe(dEncontrados =>{
        this.documentosEcontrados = dEncontrados ;
        console.log(this.documentosEcontrados);

        if(this.documentosEcontrados > 0)
      {
        console.log('XXXX');
        this.validador = 'El nombre del archivo ya está registrado';
        this.spinner.hide();
      }else{
        
        this.personalService.docUpload(fdoc);
        this.nombreDocumento = 'Documento cargado correctamente';
        //this.getPersonal(); //revisar bug
        this.spinner.hide();
      }
      });
    }else{
    
      this.validador = 'Favor de llenar los campos obligatorios';
      this.spinner.hide();
      
    }
    //this.getPersonal();
  }

  // setea formulario para nuevo registro

  public nuevo(){
    this.edicion = false;
    this.stepper1();
    this.formPersonal.reset();
    this.formPersonal.controls.departamento.setValue('');
    this.formPersonal.controls.rol.setValue('');
    this.validador = '';
    this.activo = 'true';
    this.imagen = '';
    this.imgURL = '';
    this.alertaSize = '';
  }

  public getPersonal(){
    this.spinner.show();
    this.personalService.obtienePersonal().subscribe(
      items => {
        this.listaPersonal = items.filter(usuario => usuario.cat_role.id !== 6 );
        console.log(items);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  getRoles(){
    this.spinner.show();
    this.catService.obtieneRoles().subscribe(
      items => {
        this.listaRoles = items.filter(usuario => usuario.id !== 6 && usuario.id !== 7 );
       console.log(this.listaRoles);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }


  // lista de espacios educativos

  getDepartamentos(){
    this.spinner.show();
    this.catService.obtieneDepartamentos().subscribe(
      items => {
        this.listaDepartamentos = items;
       // console.log(this.listaEspacios);
        this.spinner.hide();
      }, error => {
        if(error.status === 500){
          console.log('error de servidor');
          this.spinner.hide();
        }
      }
    )
  }

  // guarda personal

  public guardarNuevoPersonal(): void {
    this.spinner.show();
    this.validador = '';
    if(this.formPersonal.valid) {

      const fd = new FormData();
      fd.append('clave', this.formPersonal.controls.clave.value);
      fd.append('rol', this.formPersonal.controls.rol.value);
      fd.append('nombres', this.formPersonal.controls.nombres.value);
      fd.append('apellidoPat', this.formPersonal.controls.apellidoPat.value);
      fd.append('apellidoMat', this.formPersonal.controls.apellidoMat.value);
      fd.append('genero', this.formPersonal.controls.genero.value);
      fd.append('fechaNacimiento', this.formPersonal.controls.fechaNacimiento.value);
      fd.append('nacionalidad', this.formPersonal.controls.nacionalidad.value);
      fd.append('curp', this.formPersonal.controls.curp.value);
      fd.append('rfc', this.formPersonal.controls.rfc.value);
      fd.append('nss', this.formPersonal.controls.nss.value);
      fd.append('estadoCivil', this.formPersonal.controls.estadoCivil.value);
      fd.append('nombrePadre', this.formPersonal.controls.nombrePadre.value);
      fd.append('nombreMadre', this.formPersonal.controls.nombreMadre.value);
      fd.append('domicilio', this.formPersonal.controls.domicilio.value);
      fd.append('cp', this.formPersonal.controls.cp.value);
      fd.append('colonia', this.formPersonal.controls.colonia.value);
      fd.append('municipio', this.formPersonal.controls.municipio.value);
      fd.append('ciudad', this.formPersonal.controls.ciudad.value);
      fd.append('entidad', this.formPersonal.controls.entidad.value);
      fd.append('pais', this.formPersonal.controls.pais.value);
      fd.append('telefono', this.formPersonal.controls.telefono.value);
      fd.append('movil', this.formPersonal.controls.movil.value);
      fd.append('correo', this.formPersonal.controls.correo.value);
      fd.append('contrasena', this.formPersonal.controls.contrasena.value);
      fd.append('socialNet', this.formPersonal.controls.socialNet.value);
      fd.append('departamento', this.formPersonal.controls.departamento.value);
      fd.append('institucion', this.inst);
      fd.append('registradoPor', this.usu);
      window.scroll(0, 0);
      this.formPersonal.reset();
      this.formPersonal.controls.departamento.setValue('');
      this.formPersonal.controls.rol.setValue('');
      
      this.confirmaGuardado = true;
      
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.confirmaGuardado = false;
        this.stepper1();
        this.getPersonal();
        this.getRoles();
        this.imgURL = '';
        this.spinner.hide();
      }, 2500);
      
      //this.personalService.cargaImg(fd);
    this.personalService.crearPersonal(fd)
    .subscribe(response => { });
    }
    else {
      this.validador = 'Favor de llenar los campos obligatorios';
     // console.log(this.validador)
      this.spinner.hide();
    }
  }

  // funcion para editar registro
  public updatePersonal(id) {
    this.spinner.show();
    this.stepper1();
    this.validador = '';
    this.edicion = true;

    //console.log(id);
    this.idItemSeleccionado = id;
    this.registroSeleccionado = this.listaPersonal.filter(id => id.id === this.idItemSeleccionado);

    this.personalSeleccionado = this.registroSeleccionado.values;

    for (const personalSeleccionado of this.registroSeleccionado) {
      //console.log(plantelSeleccionado);
      //console.log(plantelSeleccionado.descripcion);
      this.formPersonal.controls.clave.setValue(personalSeleccionado.clave);
      this.formPersonal.controls.rol.setValue(personalSeleccionado.rol);
      this.formPersonal.controls.nombres.setValue(personalSeleccionado.nombres);
      this.formPersonal.controls.apellidoPat.setValue(personalSeleccionado.apellido_pat);
      this.formPersonal.controls.apellidoMat.setValue(personalSeleccionado.apellido_mat);
      this.formPersonal.controls.genero.setValue(personalSeleccionado.genero);
      this.formPersonal.controls.fechaNacimiento.setValue(personalSeleccionado.fecha_nacimiento);
      this.formPersonal.controls.nacionalidad.setValue(personalSeleccionado.nacionalidad);
      this.formPersonal.controls.curp.setValue(personalSeleccionado.curp);
      this.formPersonal.controls.rfc.setValue(personalSeleccionado.rfc);
      this.formPersonal.controls.nss.setValue(personalSeleccionado.nss);
      this.formPersonal.controls.estadoCivil.setValue(personalSeleccionado.estado_civil);
      this.formPersonal.controls.nombrePadre.setValue(personalSeleccionado.nombre_padre);
      this.formPersonal.controls.nombreMadre.setValue(personalSeleccionado.nombre_madre);
      this.formPersonal.controls.domicilio.setValue(personalSeleccionado.domicilio);
      this.formPersonal.controls.cp.setValue(personalSeleccionado.cp);
      this.formPersonal.controls.colonia.setValue(personalSeleccionado.colonia);
      this.formPersonal.controls.municipio.setValue(personalSeleccionado.municipio);
      this.formPersonal.controls.ciudad.setValue(personalSeleccionado.ciudad);
      this.formPersonal.controls.entidad.setValue(personalSeleccionado.entidad);
      this.formPersonal.controls.pais.setValue(personalSeleccionado.pais);
      this.formPersonal.controls.telefono.setValue(personalSeleccionado.telefono);
      this.formPersonal.controls.movil.setValue(personalSeleccionado.movil);
      this.formPersonal.controls.correo.setValue(personalSeleccionado.correo);
      this.formPersonal.controls.contrasena.setValue(personalSeleccionado.contrasena);
      this.formPersonal.controls.socialNet.setValue(personalSeleccionado.social_net);
      this.formPersonal.controls.departamento.setValue(personalSeleccionado.cat_departamento.id);

      this.idItemSeleccionado = personalSeleccionado.id;
    }



    //console.log(this.registroSeleccionado);
    this.spinner.hide();

  }

  // función para guardar cambios
  public guardarCambios(): void {
    this.spinner.show();
    //console.log(this.formPlantel.value);
    const fd = new FormData();
      console.log(this.idItemSeleccionado);
      fd.append('id', String(this.idItemSeleccionado))
      fd.append('clave', this.formPersonal.controls.clave.value);
      fd.append('rol', this.formPersonal.controls.rol.value);
      fd.append('nombres', this.formPersonal.controls.nombres.value);
      fd.append('apellidoPat', this.formPersonal.controls.apellidoPat.value);
      fd.append('apellidoMat', this.formPersonal.controls.apellidoMat.value);
      fd.append('genero', this.formPersonal.controls.genero.value);
      fd.append('fechaNacimiento', this.formPersonal.controls.fechaNacimiento.value);
      fd.append('nacionalidad', this.formPersonal.controls.nacionalidad.value);
      fd.append('curp', this.formPersonal.controls.curp.value);
      fd.append('rfc', this.formPersonal.controls.rfc.value);
      fd.append('nss', this.formPersonal.controls.nss.value);
      fd.append('estadoCivil', this.formPersonal.controls.estadoCivil.value);
      fd.append('nombrePadre', this.formPersonal.controls.nombrePadre.value);
      fd.append('nombreMadre', this.formPersonal.controls.nombreMadre.value);
      fd.append('domicilio', this.formPersonal.controls.domicilio.value);
      fd.append('cp', this.formPersonal.controls.cp.value);
      fd.append('colonia', this.formPersonal.controls.colonia.value);
      fd.append('municipio', this.formPersonal.controls.municipio.value);
      fd.append('ciudad', this.formPersonal.controls.ciudad.value);
      fd.append('entidad', this.formPersonal.controls.entidad.value);
      fd.append('pais', this.formPersonal.controls.pais.value);
      fd.append('telefono', this.formPersonal.controls.telefono.value);
      fd.append('movil', this.formPersonal.controls.movil.value);
      fd.append('correo', this.formPersonal.controls.correo.value);
      fd.append('contrasena', this.formPersonal.controls.contrasena.value);
      fd.append('socialNet', this.formPersonal.controls.socialNet.value);
      fd.append('departamento', this.formPersonal.controls.departamento.value);
      fd.append('registradoPor', this.usu);

    if (this.formPersonal.valid) { // Recibe objeto de formulario y valida
    //console.log(this.idItemSeleccionado);
    this.personalService.editarPersonal( // Envía método post al servicio con los valores del formulario
       fd
    )
    this.confirmaEdicion = true;
    this.spinner.hide();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.edicion = false;
        this.formPersonal.reset(); // Limpia campos del formulario
      this.formPersonal.controls.departamento.setValue('');
      this.formPersonal.controls.rol.setValue('');
      this.imagen = '';
      this.imgURL = '';
     // console.log('bla v+ssasa' + this.formPlantel);
      
      this.getPersonal();
      this.getRoles();
        this.stepper1();
        
        this.confirmaEdicion = false;
      }, 1000);
      
      
    } else {
      this.validador = 'Favor de llenar todos los campos'; // Si hay campos vacíos muestra alerta
      this.spinner.hide();
    }
  }

  // función para desactivar registro
  public eliminarPersonal(id): void {
    this.spinner.show();

    this.idItemSeleccionado = id;
    //console.log(this.idItemSeleccionado);
    this.personalService.desactivPersonal( // Envía método post al servicio con los valores del formulario
      {
        id:id,
        estatus: 0,
        registradoPor: this.idUsuario
      }
    ).subscribe(response => { 
      this.formPersonal.reset(); // Limpia campos del formulario
      //console.log('bla v+ssasa' + this.formPlantel);
      this.getPersonal();
      this.getRoles();
      this.p = 1;
      this.spinner.hide()
    });
  }

  public eliminarDoc(docSel):void{
    this.spinner.show();
    this.idItemSeleccionado = docSel;
    this.personalService.eliminaDocumento({
      id: docSel,
      registradoPor: this.idUsuario
    }).subscribe(response =>{
      this.getPersonal();
      this.getRoles();
      this.spinner.hide();
    })
    
  }

    // funcion para descargar pdf

    public descargarPdf() {

      var doc = new jsPDF('p', 'mm', 'A4')
      // cabeceras del reporte
  
      const addHeaders = doc => {
        const pageCount = doc.internal.getNumberOfPages()
  
        doc.setFont('helvetica', 'italic')
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
  
          doc.setPage(i)
          autoTable(doc, {
            theme: 'grid',
            margin: { top: 15 },
            head: [[{ content: 'SAPP - Directorio', colSpan: 4, styles: { halign: 'center', minCellHeight: 14, fillColor: '#fff', fontSize: 18, textColor: '#000' } }]],
            body: [
              [{ content: 'Instituto: ', styles: { fontStyle: 'bold' } }, this.institucion, { content: 'Fecha: ', styles: { fontStyle: 'bold' } }, this.formattedDate],
              [{ content: 'Usuario: ', styles: { fontStyle: 'bold' } }, this.nombreUsuario, { content: 'Hora: ', styles: { fontStyle: 'bold' } }, this.formattedTime],
              // ...
            ],
          })
          doc.text('Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 287, {
            align: 'center'
          })
          
          doc.addImage(this.logo,'png',180, 3,20,20)
        }
      }
  
        // datos del reporte
  
        addHeaders(doc)
        autoTable(doc, {theme:'grid', html: '#reportePdf', margin: { top: 50 }, headStyles: { fillColor: '#527780', fontSize: 6 }, bodyStyles: {fontSize: 6} })
        const newHead = doc => {
          const pageCount = doc.internal.getNumberOfPages()
          if(pageCount > 1) {
            addHeaders(doc)
          }
        }
        newHead(doc)
        doc.save('Directorio.pdf');
  
    }
  
    // exporta excel
  
    public exportaExcel(): void{
      this.exportExcel.exportToExcel(this.listaPersonal, 'my_export');
    }

}
